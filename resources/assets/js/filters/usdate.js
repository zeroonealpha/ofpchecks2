/**
 * Created by craigwebster on 22/02/2016.
 */

var moment = require('moment');

module.exports = (function (datetime) {

    var mydate = new moment(datetime, 'DD/MM/YYYY');

    return mydate.format('YYYY-MM-DD');

})
