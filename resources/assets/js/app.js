
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('vs-alert', vuestrapBase.alert);
Vue.component('vs-dropdown', vuestrapBase.dropdown);
Vue.component('vs-tabs', vuestrapBase.tabs);
Vue.component('vs-tab', vuestrapBase.tab);
Vue.component('datepicker', require('./components/datepicker.vue'));

var moment = require('moment');

const app = new Vue({
    el: 'body',

    filters: {
        usdate: require('./filters/usdate.js')
    },

    data: {
        period: 0,
        unit: '',
        selectedDate: '',
        original_performed_date: '',
        original_expiry_date: ''
    },

    ready: function() {
        this.selectedDate = this.original_performed_date;
        this.expiry = this.original_expiry_date;
    },


    computed: {
        expiry: function() {

            var renewed = moment(this.selectedDate,'DD/MM/YYYY');

            renewed.add(this.period,this.unit);

            renewed.subtract(1, 'days');

            if(renewed.format('DD/MM/YYYY') == "Invalid date") return "Select Date"; else return renewed.format('DD/MM/YYYY');
        }
    }
});

