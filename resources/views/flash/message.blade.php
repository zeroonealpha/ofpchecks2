@if(session()->has('flash_message'))
    <vs-alert
            :show="true"
            state="{{ session('flash_message.type') }}"
            dismissible>
        <strong>{{ session('flash_message.title') }}</strong>
        <p>{{ session('flash_message.message') }}</p>
    </vs-alert>
@endif

