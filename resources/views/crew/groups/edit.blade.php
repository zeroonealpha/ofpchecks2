@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        @include('flash.message')

        <ol class="breadcrumb">
            <li><a href="/crew/groups"><i class="fa fa-chevron-right"></i> Crew Groups</a></li>
            <li><a href="#">Add/Remove Crew</a></li>
            <li><a href="#">{{ $group->name }}</a></li>
        </ol>
        <hr>

        <form id="myform" class="form-group" method="POST" action="/crew/groups/add/{{ $group->id }}">
            {{ csrf_field() }}
            <label for="crew">Select Crew to add:</label>
            <select class="select2" multiple="multiple" name="crew[]" placeholder="Select Users">
                @foreach($crews as $crew)
                        <option value="{{ $crew->UserID }}">{{ $crew->CrewRank }}  <strong>{{ $crew->SurnamePlus }}</strong> ({{ $crew->Base_ICAO }})</option>
                @endforeach
            </select>
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Add Selected User(s)</button>
        </form>
        <hr>
        <h4>Crew belonging to group:</h4>
        <div class="row">
            @foreach($group->Crew()->get() as $crew)
                <div class="col-md-4">
                    {{ $crew->CrewRank }} <strong>{{ $crew->SurnamePlus }}</strong>&nbsp;&nbsp;
                    <a href="/crew/groups/remove/{{ $group->id }}/{{ $crew->UserID }}" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
                </div>
            @endforeach
        </div>
    </div>
</div>
@stop

