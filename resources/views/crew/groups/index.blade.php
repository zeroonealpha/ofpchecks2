@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">

        @include('flash.message')

        <ol class="breadcrumb">
            <li><a href="/crew/groups"><i class="fa fa-chevron-right"></i> Pilot & Crew Groups</a></li>
            <li><a href="#"> All</a></li>
            <li><a class="btn btn-xs btn-primary" href="/crew/groups/create">Create Grouping</a></li>
            <li><button class="btn btn-xs btn-success" data-toggle="modal" data-target="#help"><i class="fa fa-question-circle"></i> What are Pilot & Crew Groups?</button></li>
        </ol>
        <div class="col-md-12">
            <vs-tabs>
                @foreach($groups as $group)
                    <vs-tab title="{{ $group->name }}">
                        <div class="list-group-item">
                            <dl class="dl-horizontal">
                                <dt>Group Name</dt>
                                <dd><strong>{{ $group->name }}</strong></dd>
                                <dt>Colour</dt>
                                <dd><i class="{{ $group->colour }} fa fa-circle"></i></dd>
                                <dt>Description</dt>
                                <dd>{{ $group->description }}</dd>
                                <dt>Number of members</dt>
                                <dd>{{ $group->Crew()->count() }}</dd>
                            </dl>
                            <div class="flex-btn-container">
                                <div class="btn-diff"><small><em><i class="fa fa-clock-o"></i> Updated {{ $group->updated_at->diffForHumans() }}</em></small></div>
                                <div>
                                    @include('partials.editButton', [ 'action' => '/crew/groups/' . $group->id . '/edit'] )
                                </div>
                                <div>
                                    @include('partials.deleteButton',[ 'action' => '/crew/groups/' . $group->id])
                                </div>
                            </div>
                        </div>
                    </vs-tab>
                @endforeach
            </vs-tabs>
        </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="help">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">What are Pilot & Crew Groups?</h4>
            </div>
            <div class="modal-body">
                <p>
                    A Helicopter Operation has many different possible groups of Pilots & Crew. We have base pilots, training captains, dual-rated floating pilots, line trainers, cabin crew as well
                    as Chief Pilots and Co-Pilots. The list is endless.
                </p>
                <p>
                    If we have a multi-type/multi-base operation, we can create groupings as pilots for a particular base, and also we can create groups of pilots who can operate a particular type (or both). It does not
                    matter if there is an overlap between groups. Those with special qualifications - such as TRI/TRE and Winching pilots could have thier own special groups.
                </p>
                <p>
                    Once this is set-up we can apply the Qualifications required for a particular base, and also we can look at only these people on the <a href="{{ url('/checks/overview/') }}">Check & Qualification Overview </a> screen.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Thanks</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop
