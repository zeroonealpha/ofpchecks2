@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <ol class="breadcrumb">
            <li><a href="/crew/groups"><i class="fa fa-chevron-right"></i> Crew Groups</a></li>
            <li><a href="#"> Create New</a></li>
        </ol>
        <hr>

        @include('partials.errors')
        <form class="form-group" method="POST" action="/crew/groups">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <label>Group Name</label>
                    <input name="name" class="form-control" placeholder="eg Training Captains" type="text" value="{{ old('name') }}">
                </div>
                <div class="col-md-6">
                    <label>Description</label>
                    <input name="description" class="form-control" type="text" value="{{ old('description') }}">
                </div>
                <div class="col-md-6">
                    <select class="form-control" name="colour" placeholder="Select Colour">
                        {!! ColourOptionGenerator() !!}
                    </select>
                </div>
            </div>
            <button class="btn btn-primary">Create Group</button>
        </form>
    </div>
</div>
@stop
