@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3>Edit {{ $definition->name }} [Valid for {{ $definition->renewal_period }} {{ $definition->renewal_period_unit }}]</h3>
                <hr>

                @include('flash.message')
                @include('partials.errors')

                <form class="form-horizontal" method="POST" action="/checks/{{ $check->id }}/update/{{ $check->user_id }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="control-label col-md-2">Check renewed on:</label>
                        <div class="col-md-2">
                            <datepicker v-ref:renewed v-model="selectedDate" name="date_performed"></datepicker>
                        </div>

                        <!-- Unit data passed to Vue -->
                        <input type="hidden" v-model="original_performed_date" value="{{ $check->datePerformedDMY() }}">
                        <input type="hidden" v-model="original_expiry_date" value="{{ $check->expiryDateDMY() }}">
                        <input type="hidden" v-model="period" value="{{ $definition->renewal_period }}">
                        <input type="hidden" v-model="unit" value="{{ $definition->renewal_period_unit }}">

                        @if($definition->recurrent == 1)
                            <label class="control-label col-md-4 col-md-offset-1">Calculated Check Expiry (+@{{ period }} @{{ unit }}):</label>
                            <div class="col-md-3">
                                <datepicker class="datepicker" data-provide="datepicker" :value="expiry" name="expiry_date"></datepicker>
                            </div>
                        @else
                            <input type="hidden" name="expiry_date" value="0/0/0000">
                        @endif
                    </div>

                    <div class="form-group">
                        <!-- Remarks -->
                        <label class="control-label">Remarks</label>
                        <textarea name="remarks" class="form-control" rows="2" placeholder="..Remarks to operations"></textarea>
                    </div>
                    <br>
                    <div class="checkbox">
                        @if(Auth::user()->checks_approver == 0)
                            <div class="disabled">
                                <label>
                                    <input type="checkbox" name="check_approved" value="1" disabled="true" /> Approve check now?
                                </label>
                            </div>
                        @else
                            <label>
                                <input type="checkbox" name="check_approved" value="1" /> Approve check now?
                            </label>
                        @endif
                    </div>

                    <a href="/home/{{ $check->user_id }}" class="btn btn-default pull-right"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                    <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i>&nbsp;Update Check</button>
                    <br>
                    <br>
                </form>

                <!-- Scan Upload if Scan is Required-->
                @if($definition->scan_required)
                    <div class="row">
                        <div id="app" class="col-md-10 col-md-offset-1">
                            <form id="myAwesomeDropzone" role="form" method="POST" action="/checks/uploader" enctype="multipart/form-data" class="dropzone dz-clickable">
                                {{ csrf_field() }}
                                <input type="hidden" name="guid" value="{{ $check->id }}">
                                <div class="dz-message">
                                    <h4><i class="fa fa-upload"></i>&nbsp;Drag documents here to upload</h4>
                                    <span>Or click to browse</span>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop



