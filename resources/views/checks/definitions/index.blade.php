@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-chevron-right"></i> Check Definitions</a></li>
            <li><a href="#"></a>All</li>
            <li><a class="btn btn-xs btn-primary" href="/checks/definitions/create">
                    <i class="fa fa-plus-circle"></i>
                    New Check Definition
                </a></li>
        </ol>
        <div class="col-md-12">

            <!-- Checks for All Types eg Medical, CRM etc -->
            <vs-tabs size="md">
                <vs-tab id="ALL TYPES" title="ALL TYPES">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <table class="table table-striped table-condensed table-hover table-responsive">
                                <tr>
                                    <td>Check Name</td>
                                    <td>Aircaft</td>
                                    <td>Check Type</td>
                                    <td>Description</td>
                                    <td>Renewal Period</td>
                                    <td>Scan?</td>
                                    <td>Restriction</td>
                                    <td>Fly-through?</td>
                                    <td><i class="fa fa-clock-o"></i> Last Update</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>

                                @foreach($definitions as $definition)
                                    @if(!$definition->type_specific)
                                        <tr>
                                            <td><strong>{{ $definition->name }}</strong></td>
                                            <td><div class="label lb-sm label-primary">ANY TYPE</div></td>
                                            <td nowrap><small>
                                                    @if ($definition->aircraft_check)
                                                        <i class="fa fa-plane"></i> <em>AIRCRAFT CHECK <br>{{ $definition->flight_minutes_required }}/{{ $definition->ground_minutes_required }} (Flight/Ground Mins)</em><br>
                                                    @endif
                                                    @if ($definition->sim_check)
                                                        <i class="fa fa-keyboard-o"></i> <em>SIM CHECK {{ $definition->sim_minutes_required }} Simulator Minutes Required</em><br>
                                                    @endif
                                                    @if ($definition->classroom_required)
                                                        <i class="fa fa-graduation-cap"></i> <em>CLASSROOM CHECK {{ $definition->classroom_days_required }} day(s)</em><br>
                                                    @endif
                                                </small>
                                            </td>
                                            <td>{{ $definition->description }}</td>
                                            <td nowrap>@if($definition->recurrent) {{ $definition->renewal_period }} {{ $definition->renewal_period_unit }} @else Lifetime Validity @endif @if($definition->end_of_month) <b>[Valid to end of month]</b> @endif</td>
                                            <td>@if($definition->scan_required) <i class="fa fa-check"></i> @else <i class="fa fa-remove"></i> @endif</td>
                                            <td>@if($definition->generate_restriction_on_expiry) <i class="fa fa-check"></i> {{ $definition->restriction_message }} @else <i class="fa fa-remove"></i> @endif</td>
                                            <td>@if($definition->flythrough) <em>Yes</em> {{ $definition->flythough }} @else <em>No</em> @endif</td>
                                            <td><small><em>{{ $definition->updated_at->diffForHumans() }}</em></small></td>
                                            <td>@include('partials.editButton', ['ajax' => false, 'action' => '/checks/definitions/' . $definition->id . '/edit'] )</td>
                                            <td><button type="button" class="btn btn-xs btn-danger" data-toggle="modal" onclick="delCheckDefinition('{{ $definition->id }}','{{ $definition->name }}')" data-target="#confirm"><i class="fa fa-trash"></i> Delete</button></td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </li>
                    </ul>
                </vs-tab>

                @foreach($types as $type)
                <vs-tab title="{{ strtoupper($type->Type) }}">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <table class="table table-striped table-condensed table-hover table-responsive">
                                <tr>
                                    <td>Check Name</td>
                                    <td>Aircaft</td>
                                    <td>Check Type</td>
                                    <td>Description</td>
                                    <td>Renewal Period</td>
                                    <td>Scan?</td>
                                    <td>Restriction</td>
                                    <td>Fly-through?</td>
                                    <td><i class="fa fa-clock-o"></i> Last Update</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>

                                @foreach($definitions as $definition)
                                    @if($definition->type_specific && ($definition->type_id == $type->ICAO_Type))
                                        <tr>
                                            <td><strong>{{ $definition->name }}</strong></td>
                                            <td><div class="label label-primary">{{ $definition->type_id }}</div></td>
                                            <td nowrap><small>
                                                    @if ($definition->aircraft_check)
                                                        <i class="fa fa-plane"></i> <em>AIRCRAFT CHECK <br>{{ $definition->flight_minutes_required }}/{{ $definition->ground_minutes_required }} (Flight/Ground Mins)</em><br>
                                                    @endif
                                                    @if ($definition->sim_check)
                                                        <i class="fa fa-keyboard-o"></i> <em>SIM CHECK {{ $definition->sim_minutes_required }} Simulator Minutes Required</em><br>
                                                    @endif
                                                    @if ($definition->classroom_required)
                                                        <i class="fa fa-graduation-cap"></i> <em>CLASSROOM CHECK {{ $definition->classroom_days_required }} day(s)</em><br>
                                                    @endif
                                                </small>
                                            </td>
                                            <td>{{ $definition->description }}</td>
                                            <td nowrap>@if($definition->recurrent) {{ $definition->renewal_period }} {{ $definition->renewal_period_unit }} @else Lifetime Validity @endif @if($definition->end_of_month) <b>[Valid to end of month]</b> @endif</td>
                                            <td>@if($definition->scan_required) <i class="fa fa-check"></i> @else <i class="fa fa-remove"></i> @endif</td>
                                            <td>@if($definition->generate_restriction_on_expiry) <i class="fa fa-check"></i> {{ $definition->restriction_message }} @else <i class="fa fa-remove"></i> @endif</td>
                                            <td>@if($definition->flythrough) <em>Yes</em> {{ $definition->flythough }} @else <em>No</em> @endif</td>
                                            <td><small><em>{{ $definition->updated_at->diffForHumans() }}</em></small></td>
                                            <td>@include('partials.editButton', ['ajax' => false, 'action' => '/checks/definitions/' . $definition->id . '/edit'] )</td>
                                            <td><button type="button" class="btn btn-xs btn-danger" data-toggle="modal" onclick="delCheckDefinition('{{ $definition->id }}','{{ $definition->name }}')" data-target="#confirm"><i class="fa fa-trash"></i> Delete</button></td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </li>
                    </ul>
                </vs-tab>
                @endforeach
            </vs-tabs>
        </div>
    </div>
</div>
<form method="POST" action="/checks/definitions/delete">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <input id="definition_id" type="hidden" name="definition_id">
    <div class="modal fade" tabindex="-1" role="dialog" id="confirm">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Check Definition "<span id="definition_name"></span>"</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Check Definition?</p>
                    <strong class="text-danger"><em>PLEASE NOTE:</em> Deleting this will also delete ALL the checks associated with this definition !!</strong>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Yes, Delete</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>

@stop
@section('javascripts')
<script>
    function delCheckDefinition(id,name) {
        $("#definition_id").val(id);
        $('#definition_name').html(name);
    }
</script>
@stop


