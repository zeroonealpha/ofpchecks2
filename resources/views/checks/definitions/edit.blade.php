@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-chevron-right"></i> Check Definition</a></li>
                <li><a href="#">{{ $definition->name }}</a></li>
                <li><a href="#"></a>Edit</li>
            </ol>
            <hr>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-10 col-md-offset-1">
                <form id="myform" method="POST" action="/checks/definitions/{{ $definition->id }}/update">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Check Name</label>
                        <input class="form-control" name="name" placeholder="eg Medical / Dangerous Good" type="text" value="{{ $definition->name }}">
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <input class="form-control" name="description" placeholder="Description of check" type="text" value="{{ $definition->description }}">
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="scan_required" value="0">
                            <input type="checkbox" name="scan_required" value="1" @if($definition->scan_required) checked @endif/> Certificate Scan Required?
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="generate_restriction_on_expiry" value="0">
                            <input type="checkbox" name="generate_restriction_on_expiry" value="1" @if($definition->generate_restriction_on_expiry) checked @endif/> Expiry of Check generates a Restriction instead of Failure
                        </label>
                    </div>

                    <div class="form-group">
                        <label>Restriction Message</label>
                        <input class="form-control" name="restriction_message" placeholder="eg Must fly with LTC" type="text" value="{{ $definition->restriction_message }}">
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="flythrough" value="0">
                            <input type="checkbox" name="flythrough" value="1" @if($definition->flythrough) checked @endif/> If this check is invalid flight can only take place with an authorised signatory.
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="type_specific_checkbox" name="type_specific" value="0">
                            <input type="checkbox" id="type_specific_checkbox" name="type_specific" value="1" v-model="checked" @if($definition->type_specific) checked @endif/> Applies to Type:
                        </label>
                        <select name="type_id" placeholder="Select Type" v-show="checked" class="form-control">
                            @foreach($types as $type)
                                <option @if($definition->type_id == $type->ICAO_Type) selected @endif value="{{ $type->ICAO_Type }}">{{ $type->Type }}</option>
                            @endforeach
                        </select>
                    </div>

                    <hr>

                    <div class="row form-horizontal">
                        <div class="checkbox col-md-2">
                            <label>
                                <input type="hidden" name="aircraft_check" value="0">
                                <input type="checkbox" name="aircraft_check" value="1" @if($definition->aircraft_check) checked @endif/> Flight conducted on live aircraft?
                            </label>
                        </div>
                        <label class="control-label col-md-3">Approx. Flight Time Required</label>
                        <div class="col-md-2">
                            <input class="form-control" name="flight_minutes_required" placeholder="Time in Minutes" type="text" value="{{ $definition->flight_minutes_required }}">
                        </div>
                        <label class="control-label col-md-3">Approx. Ground Time Required</label>
                        <div class="col-md-2">
                            <input class="form-control" name="ground_minutes_required" placeholder="Time in Minutes" type="text" value="{{ $definition->ground_minutes_required }}">
                        </div>
                    </div>

                    <div class="row form-horizontal">
                        <div class="checkbox col-md-2">
                            <label>
                                <input type="hidden" name="sim_check" value="0">
                                <input type="checkbox" name="sim_check" value="1" @if($definition->sim_check) checked @endif /> Flight conducted on Simulator?
                            </label>
                        </div>

                        <label class="control-label col-md-3">Approx. Sim. Time Required</label>
                        <div class="col-md-2">
                            <input class="form-control" name="sim_minutes_required" placeholder="Time in Minutes" type="text" value="{{ $definition->sim_minutes_required }}">
                        </div>
                    </div>

                    <hr>
                    <div class="row form-inline">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    <input type="hidden" name="recurrent" value="0">
                                    <input type="checkbox" name="recurrent" value="1" @if($definition->recurrent) checked @endif /> Check requires renewal?
                                </label>
                            </div>
                            <input class="form-control" name="renewal_period" placeholder="Number of " type="text" value="{{ $definition->renewal_period }}">
                            <select name="renewal_period_unit" placeholder="Days/Months/Years" class="form-control">
                                <option value="days" @if($definition->renewal_period_unit == "days") selected @endif>Days</option>
                                <option value="months" @if($definition->renewal_period_unit == "months") selected @endif>Months</option>
                                <option value="years" @if($definition->renewal_period_unit == "years") selected @endif>Years</option>
                            </select>
                        </div>
                    </div>

                    <hr>
                    <div class="row form-horizontal">
                        <div class="checkbox col-md-2">
                            <label>
                                <input type="hidden" name="classroom_required" value="0">
                                <input type="checkbox" name="classroom_required" value="1" @if($definition->classroom_required) checked @endif/> Check requires classroom?
                            </label>
                        </div>

                        <label class="control-label col-md-3">Classroom Days Req'd</label>
                        <div class="col-md-2">
                            <input class="form-control" name="classroom_days_required" placeholder="eg 2" type="text" value="{{ $definition->classroom_days_required }}">
                        </div>
                    </div>
                    <hr>
                    <button class="btn btn-primary" type="submit">Update</button>
                    <a href="/checks/definitions" class="btn btn-danger">Cancel</a>
                </form>
            </div>
        </div>
    </div>
@stop




