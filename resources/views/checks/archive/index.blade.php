@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-chevron-right"></i> Check Renewal Archive</a></li>
                <li><a href="#"></a>View</li>
            </ol>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Check archive for {{ $crew->SurnamePlus }}'s {{ $definition->name }}
                        <span class="pull-right yellow">MOST RECENT CHECK IS BASED ON PERFORMED ON DATE</span>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive table-bordered">
                            <tr>
                                <td>Check Submitted</td>
                                <td>Check Performed</td>
                                <td>Check Expiry Date</td>
                                <td>Approved By</td>
                                <td>Remarks</td>
                                <td>Documents</td>
                                <td>Edit</td>
                                <td>Delete</td>
                            </tr>
                            @foreach($checks as $check)
                                <tr>
                                    <td>{{ $check->createdAtFormatted() }}</td>
                                    <td>{{ $check->datePerformedFormatted() }}</td>
                                    <td>
                                        {!! $check->expiryDateWithLifetime() !!}
                                    </td>
                                    <td>{{ $check->Approver() }}</td>
                                    <td>{{ $check->remarks }}</td>
                                    <td>
                                        @foreach($check->Scans()->get() as $scan)
                                            <a href="{{ createPreSignedRequestForScan($scan->id) }}" target="_blank"><i class="fa fa-file-archive-o"></i> {{ $scan->original_filename }}</a>&nbsp;
                                        @endforeach
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-warning" href="/checks/{{ $check->id }}/edit/{{ $crew_id }}"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-danger" href="/checks/{{ $check->id }}/delete/{{ $crew_id }}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <a href="/home/{{ $crew_id }}" class="btn btn-sm btn-default"><i class="fa fa-undo"></i> Back to {{ $crew->SurnamePlus }}'s Check Overview</a>
            </div>
        </div>
    </div>
@stop



