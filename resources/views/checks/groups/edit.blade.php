@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('flash.message')

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-chevron-right"></i> Qualification Groups</a></li>
                <li><a href="#">Edit Group Properties for {{ $group->name }}</a></li>
            </ol>
            <hr>
            <form class="form-group" action="/checks/groups/add" method="POST">
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                {{ csrf_field() }}
                @if( $group->Extensions()->count() > 0)
                    <li class="list-group-item"><i class="fa fa-info-circle"></i> THIS GROUP ALREADY EXTENDS
                        @foreach($group->Extensions()->get() as $extension)
                            <i class="fa fa-chevron-right {{ $extension->colour }}"></i>&nbsp;{{ $extension->name }} <a href="/checks/groups/{{ $group->id }}/remove/{{ $extension->id }}"><i class="fa fa-remove"></i></a>
                        @endforeach
                    </li>
                    <hr>
                @endif

                <label for="extends" class="required">Extend Qualification Group <i class="help icon" data-content="An extended Qualification Group requires both parent and child to be valid."></i> </label>
                <select class="select2" name="groups[]" multiple="multiple" id="extends" style="width:50%">
                    @foreach($other_groups as $other)
                        <option value="{{ $other->id }}">{{ $other->name }}</option>
                    @endforeach
                </select>

                <br><hr>

                <label for="check_select">Checks Available: </label>
                <select class="select2" name="checks[]" multiple="multiple" id="check_select" style="width:50%">
                    @foreach($definitions as $definition)
                        <option value="{{ $definition->id }}">{{ $definition->name }} @if($definition->type_specific) <strong>[{{ $definition->type_id }}]</strong> @endif</option>
                    @endforeach
                </select>

                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-save"></i>
                    Apply Changes
                </button>
                <a href="/checks/groups" class="btn btn-danger">Back to Qualification Groups</a>
                <br>
            </form>
        </div>
    </div>

    <hr>
    <h4>Existing Checks associated with this  Qualification Group ({{ count($group->CheckDefinitions) }}):</h4>
    <div class="row">
        <div class="list-group">
            @foreach($group->CheckDefinitions as $def)
                <div class="col-md-4">
                    <div class="list-group-item">
                        <i class="fa fa-clipboard"></i>&nbsp;{{ $def->name }}
                        <a href="/checks/groups/delete/check/{{ $group->id }}/{{ $def->id }}"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</div>
@stop



