@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        @include('flash.message')

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-chevron-right"></i> Qualification Groups</a></li>
            <li><a href="#"></a>All</li>
        </ol>
        <button class="btn btn-success" data-toggle="modal" data-target="#help"><i class="fa fa-question-circle"></i> What are Qualification Groups?</button>
        <a class="btn btn-primary" href="/checks/groups/create"><i class="fa fa-plus-circle"></i> Create New Group</a>
        <hr>
        @foreach($groups->chunk(3) as $chunk)
            <div class="row">
                <ul class="list-group text-left">
                    @foreach($chunk as $group)
                        <div class="col-md-4">
                            <li class="list-group-item">
                                <h4><i class="{{ $group->colour }} fa fa-circle"></i>&nbsp;{{ $group->name }}</h4>
                                <ul class="list-group">
                                    @if( $group->Extensions()->count() > 0)
                                        <li class="list-group-item"><i class="fa fa-chevron-right"></i> EXTENDS
                                            @foreach($group->Extensions()->get() as $extension)
                                                <i class="fa fa-chevron-right"></i>&nbsp;<a class="{{ $extension->colour }}">{{ $extension->name }}</a>
                                            @endforeach
                                        </li>
                                    @endif
                                </ul>
                                <div class="well">
                                    <strong>{{ $group->description }}</strong><br>
                                </div>
                                <em>Applied to Crew Groups:</em>
                                <ul class="list-inline">
                                    @if(count($group->CrewGroupsAppliedTo()) == 0) None @endif

                                    @foreach($group->CrewGroupsAppliedTo() as $crewGroup)
                                        <li class="btn btn-xs btn-default">{{ $crewGroup->name }} <a href="/crew/groups/{{ $group->id }}/removeFrom/{{ $crewGroup->id }}"> <i class="fa fa-trash"></i></a></li>
                                    @endforeach
                                </ul>
                                <hr>
                                <i class="fa fa-clipboard"></i>
                                {{ $group->CheckDefinitions()->count() }} Checks&nbsp;
                                <br><br>
                                <div class="flex-btn-container">
                                    <button type="button" class="btn btn-xs btn-success" data-toggle="dropdown">
                                        Apply to Crew Group
                                        <span class="caret"></span>
                                    </button>
                                    <ul name="dropdown-menu" class="dropdown-menu">
                                        @foreach($crew_groups as $grp)
                                            <li><a href="/crew/groups/{{ $group->id }}/applyTo/{{ $grp->id }}">{{ $grp->name }}</a></li>
                                        @endforeach
                                    </ul>
                                    <a class="btn btn-xs btn-primary" href="/checks/groups/{{ $group->id }}/edit/">&nbsp;Add/Remove Checks</a>
                                    <a class="btn btn-xs btn-danger" href="/checks/groups/{{ $group->id }}/delete/"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                                </div>
                            </li>
                        </div>
                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="help">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">What are Qualification Groups?</h4>
            </div>
            <div class="modal-body">
                <p>
                    In a modern multi-type/multi-role Helicopter Operation, there can be a large amount of different checks, each relatiing to Aircraft Types, Roles, Pilots or Crewmembers. Some checks may apply only to specific group of people. Some checks may apply to a particular type of aircraft. Not all checks apply to
                    everyone.
                </p>
                <p>Therefore, it makes sense to be able to arrange checks into 'qualification groups' which relate to a role. For example, you can create a group for an Aircraft Type-Rating, which contains all the elements of
                flying training required to fly that aircraft.</p>
                <p>In addition, extra qualifications, such as winching/load lifting or Type Rating Examiner can be added 'on-top' of another group by extending from a Type Rating group. This makes a lot of
                sense, because then we can see that perhaps a pilot has expired winching qualifications, but is still able to fly an aircraft offshore with passengers.</p>
                <p>Expanding the concept further, non-type specific checks (such as Medical, CRM etc) can be grouped together in a common base-group, to avoid repetition in higher groups. </p>
                <p>Once qualification groups have been created, it is important to then 'Apply' these to Pilots & Crewmembers using the 'Apply to Crew Group' button. Each qualification group can be applied to as many crew groups as is needed. Any overlap is automatically resolved. Crew Groups can be by
                base, type, trainers, or those with dual-ratings. It is a quick way to apply a larger amount to checks to a large number of pilots & crew at a single click.</p>
                <p>Once Checks have been definined, it is possible to delete qualification groups, as the underlying definitions will not be deleted. New groups can be created and the checks can be re-organised. The exact configuration can be
                adjusted to suit the operation.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Thanks</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop




