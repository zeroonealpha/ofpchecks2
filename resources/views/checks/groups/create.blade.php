@extends('layouts.app');

@section('content')
<div class="container">
    <div class="container">

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-chevron-right"></i> Qualification Groups</a></li>
            <li><a href="#">Create</a></li>
        </ol>
        <hr>
        <form id="myform" class="form-group" method="POST" action="/checks/groups/create">
            {{ csrf_field() }}

            <label>Qualification Group Name*</label>
            <input class="form-control" name="name" placeholder="eg Commercial Pilot VFR" type="text">

            <label>Description*</label>
            <input class="form-control" name="description" placeholder="eg Basic Company Requirements for CAT flight" type="text">

            <label>Display Order (lower numbers will be displayed first)</label>
            <select class="form-control" name="display_order" placeholder="Order of display">
                {!! DisplayOrderOptionGenerator() !!}}}
            </select>

            <br>
            <select class="form-control" name="colour" placeholder="Red">
                {!! ColourOptionGenerator() !!}
            </select>

            <button class="btn btn-primary" type="submit">Save</button>
        </form>
    </div>
</div>
@stop

