<form
    method="POST"
    action="{{ $action }}"
>
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button type="submit" type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
</form>


