<div class="col-md-4">
    <div class="list-group-item">
        @if($check = $definition->LatestForUser($crew))
        <div style="padding: 12px;" class="list-group-item-heading @if($check->status == VALID_FOREVER) list-group-item-success @endif @if(($check->status == VALID)) list-group-item-success @endif @if($check->status == VALID_WITH_RESTRICTION) list-group-item-warning @endif @if(($check->status == INVALID) || ($check->status == VALID_PENDING_APPROVAL)) list-group-item-danger @endif">
        @endif
        @if($check)
            <h4><a href="/checks/{{ $definition->id }}/archive/{{ $crew->UserID }}" title="View Archive"><i class="fa fa-clipboard"></i></a> <strong>{{ $definition->name }}</strong>&nbsp
                @if($check->status == VALID_FOREVER)
                    <i class="green fa fa-check"></i>
                    <small class="pull-right approved"><i class="fa fa-certificate"></i> Approved by {{ $check->Approver() }}</small>
                @endif
                @if (($check->status) == VALID)
                    <i class="green fa fa-check"></i>
                    <small class="pull-right approved"><i class="fa fa-certificate"></i> Approved by {{ $check->Approver() }}</small>
                @endif
                @if (($check->status) == PREVIOUS_CHECK_VALID_LATEST_PENDING_APPROVAL)
                    <i class="green fa fa-check"></i>
                    <small class="pull-right pending"><i class="fa fa-hourglass-start"></i> Pending Approval (validity covered by previous check)</small>
                @endif
                @if (($check->status) == VALID_PENDING_APPROVAL)
                    <i class="red fa fa-remove"></i>
                    @if ($check->NumberOfApprovals() == 0 )
                        <small class="pull-right pending"><i class="fa fa-hourglass-start"></i> No Approvals Yet</small>
                    @endif
                    @if ($check->NumberOfApprovals() == 1 )
                        <small class="pull-right pending"><i class="fa fa-hourglass-start"></i> 1 Approval by {{ $check->Approver() }}</small>
                    @endif
                @endif
                @if (($check->status) == VALID_WITH_RESTRICTION)
                    <i class="orange fa fa-exclamation-triangle"></i>
                    @if ($check->NumberOfApprovals() == 0 )
                        <small class="pull-right pending"><i class="fa fa-hourglass-start"></i> No Approvals Yet</small>
                    @endif
                    @if ($check->NumberOfApprovals() == 1 )
                        <small class="pull-right pending"><i class="fa fa-hourglass-start"></i> 1 Approval by {{ $check->Approver() }}</small>
                    @endif
                @endif
                @if (($check->status) == INVALID)
                    <i class="red fa fa-remove"></i><small class="pull-right"></small>
                @endif
            </h4>
        </div>
        <small>
            @if($check->isRecurrent())
                Renewed {{ toDate($check->date_performed) }} - {!! expiry($check->expiry_date, $check->expires_in) !!}<br>
            @else
                Valid from {{ toDate($check->date_performed) }}
            @endif
            @if (($check->status) == VALID_WITH_RESTRICTION)
                <br><small style="color: darkred;">[Restriction Applies: {{ $definition->restriction_message }}]</small>
                @else
                <br><small style="color: green;">[No Restriction]</small>
            @endif
            <br>
            <i class="fa fa-paperclip"></i> Documents ({{ $check->Scans()->count() }})

            @foreach($check->Scans()->get() as $scan)
                , <a href="{{ createPreSignedRequestForScan($scan->id) }}">{{ $scan->original_filename }}</a>
            @endforeach

            <a class="btn btn-xs btn-primary pull-right" href="/checks/{{ $definition->id }}/revalidate/{{ $crew->UserID }}"><i class="fa fa-refresh"></i> Revalidate</a>

            @if($check->UserHasApproved())
                <a class="btn btn-xs btn-default pull-right disabled" href="/checks/{{ $check->id }}/approve/{{ $crew->UserID }}"><i class="fa fa-check"></i> Approved</a>&nbsp;
            @else
                @if (($check->NumberOfApprovals() < 2) && Auth::user()->checks_approver)
                    <a class="btn btn-xs btn-success pull-right" href="/checks/{{ $check->id }}/approve/{{ $crew->UserID }}"><i class="fa fa-thumbs-up"></i> Approve</a>&nbsp;
                @endif
            @endif

        </small>
        @else
        <div class="list-group-item-heading list-group-item-danger" style="padding: 12px;" >
        @if($definition->generate_restriction_on_expiry)
                <h4><i class="fa fa-clipboard"></i> <strong>{{ $definition->name }}</strong> <i class="orange fa fa-exclamation-triangle"></i></h4></div>
            @else
                <h4><i class="fa fa-clipboard"></i> <strong>{{ $definition->name }}</strong> <i class="red fa fa-remove"></i></h4></div>
                @endif
                <small><strong>NOT RECORDED</strong>
                    @if($definition->generate_restriction_on_expiry)
                        <br><small style="color: darkred;">[Restriction Applies: {{ $definition->restriction_message }}]</small>
                        @else
                        <br><small style="color: green;">[No Restriction]</small>
                    @endif
                    <br><i class="fa fa-paperclip"></i> Documents (0), <a href="#">none</a>
                    <a class="btn btn-xs btn-primary pull-right" href="/checks/{{ $definition->id }}/revalidate/{{ $crew->UserID }}"><i class="fa fa-refresh"></i> Revalidate</a>
                </small>
            @endif
    </div>
</div>


