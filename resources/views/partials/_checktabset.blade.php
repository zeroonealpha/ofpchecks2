<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
    @foreach($crew->checkGroups()->orderBy('display_order')->get() as $group)
        <li role="presentation" @if($loop->first) class="active" @endif><a href="#{{ $group->id }}" aria-controls="home" role="tab" data-toggle="tab"><strong> {{ $group->name }} {!! $group->message() !!}</strong></a></li>
    @endforeach
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        @foreach($crew->checkGroups()->get() as $group)
            <div role="tabpanel" class="tab-pane @if($loop->first) active @endif>" id="{{ $group->id }}">
                <div class="row">
                @foreach($group->CheckDefinitions()->orderBy('name')->get() as $definition)
                    <!-- DISPLAY CHECKS FOR THIS GROUP -->
                    @include('partials._definition', ['check' => $definition->LatestForUser($crew), 'definition' => $definition])
                @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>



