<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/img/favicon.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Checks') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/select2.min.css" rel="stylesheet" />
    <link href="/css/dropzone.min.css" rel="stylesheet" />
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet" />

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="logo visible-md visible-lg" href="#"><img src="/img/fss-logo.png" style="max-width: 300px"></a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                @if(Auth::check())
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Dashboard</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Pilots & Crew <span class="caret"></span>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/crew/groups/') }}">
                                        Groupings
                                    </a>
                                </li>
                            </ul>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Checks & Qualifications <span class="caret"></span>
                            <ul class="dropdown-menu" role="menu">
                                <li style="width: 100%;">
                                    <a href="{{ url('/checks/definitions/') }}">
                                        Check Definitions
                                    </a>
                                </li>
                                <li style="width: 100%;">
                                    <a href="{{ url('/checks/groups/') }}">
                                        Qualification Groups
                                    </a>
                                </li>
                                @foreach(\App\CheckGroup::forOperator() as $qual)
                                <li style="width: 100%;">
                                    <a href="{{ url('/checks/overview/' . $qual->id) }}">
                                        {{ $qual->name }} Overview
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </a>
                    </li>
                </ul>
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->firstname }} {{ Auth::user()->lastname }} [{{ Auth::user()->base_icao }}]<span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="/js/all.js"></script>

    @yield('javascripts')
    <script>
        $('div.alert').delay(3000).slideUp(300);
        $(".select2").select2();

        Dropzone.options.myAwesomeDropzone = {
            paramName: "upload", // The name that will be used to transfer the file
            maxFilesize: 50, // MB
            //addRemoveLinks: true,
            uploadMultiple: true,
            maxFiles: 10,
            acceptedFiles: ".heic, .hevc, .heif, .pdf, .png, .gif, .jpg, .jpeg, .doc, .docx, application/msword, application/pdf,application/*,application/vnd.ms-excel",
        };

        $('.datepicker').datepicker({
                    format: 'dd/mm/yyyy'
        });
    </script>
</body>
</html>

