@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">

    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-chevron-right"></i> Check & Qualifications</a></li>
        <li><a href="#"></a>Overview</li>
    </ol>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-share"></i>&nbsp;Showing {{ $checkgroup->name }} Overview</strong>
            </div>
            <div class="panel-body" style="overflow-x: scroll">
                <table class="table table-striped table-hover table-bordered">
                    <tr>
                        <td style="width: min-content;" rowspan="2">Rank</td>
                        <td rowspan="2">Name</td>
                        <td class="text-center" colspan="{{ $checkgroup->numberOfChecks() }}"><span style="color: {{ $checkgroup->colour }}" class="fa fa-circle"></span> {{ $checkgroup->name }}</td>
                    </tr>
                    <tr>
                        @foreach($checkgroup->CheckDefinitions as $def)
                        <td class="text-center">{{ $def->name }}</td>
                        @endforeach
                    </tr>
                    @foreach($crews as $crew)
                        @if($crew->notAccessibleByUser()) @continue @endif
                        @if(!$crew->isInCheckGroup($checkgroup)) @continue @endif
                        <tr>
                        <td><strong>{{ $crew->CrewRank }}</strong></td>
                        <td>{{ $crew->SurnamePlus }} <a href="/home/{{ $crew->UserID }}"><i class="fa fa-search-plus"></i></a></td>
                            @foreach($checkgroup->CheckDefinitions as $def)
                                @if($check = $def->LatestForUserWithoutUpdate($crew))
                                    <td class="text-center" nowrap><span style="cursor: pointer;" title="{{ $crew->SurnamePlus }} - {{ $def->name }}">{!! $check->FormattedStatus() !!}</span></td>
                                @else
                                    <td class="text-center" nowrap><span style="color: lightgray;"><span class="fa fa-question-circle"></span></span></td>
                                @endif
                                @php $check = null @endphp
                            @endforeach
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@stop
