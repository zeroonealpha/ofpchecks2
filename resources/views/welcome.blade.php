@extends('layouts.app')

@section('content')
<div class="container">
    <div class="jumbotron">
        <h1><span class="glyphicon glyphicon-ok green"></span> Checks</h1>
        <p>Create Checks <i class="fa fa-arrow-right"></i> Arrange into groups <i class="fa fa-arrow-right"></i> Apply to Pilots</p>
        <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a></p>
        <small>&copy; Flight Software Services Ltd {{ \Carbon\Carbon::now()->year }}</small>
    </div>
</div>
@stop
