@extends('layouts.app')

<!-- Page Contents -->
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-chevron-right"></i> Inspect Crew</a></li>
            <li><a href="#">{{ $crew->CrewRank }} <strong>{{ $crew->SurnamePlus }}</strong> [{{ $crew->CrewCode }}]</a></li>
        </ol>

        @include('flash.message')

        <div class="btn-group">
            <form method="POST" action="\home">
                {{ csrf_field() }}
                <label for="crew">Select Crew</label>
                <select class="select2" name="crew" placeholder="Select Users">
                    @foreach($crews as $member)
                        @if($member->Base_ICAO == 'VTSH')
                            @continue
                        @endif
                        @if($member->UserID == $crew->UserID)
                            <option selected value="{{ $member->UserID }}">{{ $member->CrewRank }} <strong>{{ $member->SurnamePlus }}</strong> <em>({{ $member->Base }})</em></option>
                        @else
                            <option value="{{ $member->UserID }}">{{ $member->CrewRank }} <strong>{{ $member->SurnamePlus }}</strong> <em>({{ $member->Base }})</em></option>
                        @endif
                        @endforeach
                </select>
                <button type="submit" class="btn btn-xs btn-primary">Go</button>
            </form>
        </div>
        <hr>
        <div class="panel panel-default panel-primary">
            <div class="panel-heading">
                <h4>
                    {{ $crew->CrewRank }} <strong>{{ $crew->SurnamePlus }}</strong> [{{ $crew->CrewCode }}]
                </h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive img-rounded mugshot" src="/img/crew/mugshot_blank.png">
                    </div>
                    <div class="col-md-6">
                        <dl class="dl-horizontal">
                            <dt>Licence #</dt>
                            <dd>{{ $crew->LicenseNo }}</dd>
                            <dt>Employee #</dt>
                            <dd>{{ $crew->Employee_Number }}</dd>
                            <dt>Email</dt>
                            <dd>{{ $crew->email }}</dd>
                            <dt>Phone</dt>
                            <dd>{{ $crew->Phone }}</dd>
                            <dt>Roster Schedule</dt>
                            <dd>{{ $crew->Schedule }}</dd>
                            <dt>Weight</dt>
                            <dd>{{ $crew->Weightinlbs }} lbs</dd>
                            <dt>Date of Birth</dt>
                            <dd>{{ $crew->DOB }} ({{ $crew->age() }})</dd>
                        </dl>
                    </div>
                    <div class="col-md-3">
                        <strong>Crew Groups:</strong>
                        <ul style="list-style: none;">
                            @foreach($crew->Groups()->get() as $group)
                                <li><i class="fa fa-circle {{ $group->colour }}"></i> {{ $group->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                @include('partials._checktabset', ['crew' => $crew])
            </div>
        </div>
    </div>
</div>
<form method="POST" action="/crew/photo/upload">
    {{ csrf_field() }}
    <input id="crew_id" type="hidden" name="{{  $crew->UserID }}">
    <div class="modal fade" tabindex="-1" role="dialog" id="confirm">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload Crew Photo</h4>
                </div>
                <div class="modal-body">
                    <p>Select a photo to upload and it will be resized and stored.</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Upload</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>

@stop

