@extends('layouts.app')

<!-- Page Contents -->
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-chevron-right"></i> Inspect Crew</a></li>
                </ol>

                @include('flash.message')

                <div class="panel panel-danger">
                    <div class="panel-body">
                        There are no Crew defined on the system yet !
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop



