<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User')->create(['operator_id' => 'weststar', 'email' => 'ops@test.com', 'role' => 1]);
        factory('App\User')->create(['operator_id' => 'weststar', 'email' => 'ops2@test.com', 'role' => 1]);
    }
}

