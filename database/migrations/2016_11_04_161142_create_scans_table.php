<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scans', function(Blueprint $table) {
            $table->string('id')->primary();
            $table->string('scanable_id');      // Polymorphic Relation with either Checks or Opscoms etc.
            $table->string('scanable_type');
            $table->string('original_filename');
            $table->string('filename');
            $table->string('mimetype');
            $table->boolean('uploaded_ok')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scans');
    }
}

