var elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/', 'public/fonts/bootstrap/');
    mix.copy('node_modules/font-awesome/fonts/', 'public/fonts/');

    // Css
    mix.copy('node_modules/select2/dist/css/select2.min.css', 'public/css/select2.min.css');
    mix.copy('node_modules/dropzone/dist/min/dropzone.min.css', 'public/css/dropzone.min.css');
    mix.copy('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', 'public/css/bootstrap-datepicker.min.css');

    // Js
    mix.copy('node_modules/select2/dist/js/select2.min.js', 'resources/assets/js/select2.min.js');
    mix.copy('node_modules/dropzone/dist/min/dropzone.min.js', 'resources/assets/js/dropzone.min.js');
    mix.copy('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', 'resources/assets/js/bootstrap-datepicker.min.js');

    // Combine to all.js in public/js
    mix.scripts(['select2.min.js', 'dropzone.min.js','bootstrap-datepicker.min.js']);

    mix.webpack('app.js');
});
