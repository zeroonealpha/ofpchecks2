<?php namespace App\Jobs;

use App\User;
use App\Jobs\Job;
use App\Operator;
use App\Repositories\CheckRepository;
use Illuminate\Contracts\Bus\SelfHandling;

// L5.2 Self Handling by default
class CalculateCheckGroup extends Job
{
    // User id of group to calculate
    protected $repository;

    /**
     * CalculateCheckGroup constructor.
     * @param CheckRepository $repository
     */
    public function __construct(CheckRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = Operator::this()->Users()->get();

        $this->repository->calculateAllChecksForUsers($users);
    }
}
