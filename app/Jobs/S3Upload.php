<?php namespace App\Jobs;

use Log;
use Storage;
use App\User;
use App\Scan;
use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Filesystem\Cloud;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class S3Upload
 * @package App\Jobs
 */
class S3Upload extends Job implements ShouldQueue
{
    use InteractsWithQueue;

    protected $document;
    protected $filename;

    /**
     * @param $filename
     */
    public function __construct($filename)
    {
        // Retrieve the uploaded document from the temporary store
        $this->filename =  $filename;
    }

    /**
     * Handle the upload to S3, and log the result.
     */
    public function handle()
    {
        $this->upload('s3');
        $this->deleteTemporaryFile();
        $this->markAsUploadedOK();

        // Log the event
        Log::info('File:' . $this->filename . ' successfully uploaded to S3');
    }

    /**
     * @param $target
     */
    private function upload($target)
    {
        Storage::disk($target)->put($this->filename, file_get_contents(storage_path() . '/tmp/' . $this->filename));
    }

    /**
     * Delete from /tmp/
     */
    private function deleteTemporaryFile()
    {
        Storage::disk('local')->delete('/tmp/' . $this->filename);
    }

    /**
     * Mark scan table as OK
     */
    private function markAsUploadedOK()
    {
        Scan::where('filename', $this->filename)->update(['uploaded_ok' => 1]);
    }

    /**
     * If upload fails, log the occurrence.
     */
    public function failed()
    {
        Log::info('File:' . $this->filename . ' failed upload to S3');
    }
}


