<?php namespace App;

use Auth;
use App\Traits\RemoteConnection;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Crew extends Model
{

    use RemoteConnection;

    //protected $dates = ['DOB'];
    protected $primaryKey = "UserID";
    public $incrementing = false;
    protected $table = 'crews';

    public function age()
    {
        // Test for date format - or /
        if(str_contains($this->DOB, '-')) {
            $dob = Carbon::createFromFormat('d-m-Y', $this->DOB);
        } elseif(str_contains($this->DOB, '/')) {
            $dob = Carbon::createFromFormat('d/m/Y', $this->DOB);
        }

        if($dob) return 'Age ' . $dob->diffInYears(Carbon::now());

        return 0;
    }

    public function Checks()
    {
        return $this->hasMany('App\Check', 'user_id', 'UserID');
    }

    public function Groups()
    {
        return $this->belongsToMany('App\Group', 'crew_group', 'crew_id', 'group_id');
    }

    public function checkGroups()
    {
        return $this->belongsToMany('App\CheckGroup','check_groups_users', 'user_id', 'group_id')->withPivot('group_valid', 'check_messages', 'recency_messages', 'check_summary')->orderBy('display_order');
    }

    public static function forOperator()
    {
        // Filter out bases?
        $user = Auth::user();

        if(!$user->all_base_access) {
            $basestring = Auth::user()->base_icao;

            $base_array = explode("-", $basestring);

            return static::where('Deleted', 'No')->whereIn('Base_ICAO', $base_array)->orderBy('SurnamePlus');
        }

        return static::where('Deleted', 'No')->orderBy('SurnamePlus');
    }

    public function notAccessibleByUser()
    {
        // Is this crew's bases compatible with the Ops Users privileges?
        // Filter out bases?
        $user = Auth::user();

        if(!$user->all_base_access) {
            $basestring = Auth::user()->base_icao;

            $base_array = explode("-", $basestring);

            if(!in_array($this->Base_ICAO, $base_array)) return true;
        }

        return false;
    }

    public function isInGroup($group)
    {
        return $this->Groups()->get()->contains($group);
    }

    public function isInCheckGroup($group)
    {
        return $this->CheckGroups()->get()->contains($group);
    }

    public function nextCheck()
    {
        $checks = $this->Checks()->where('latest', 1)->where('renewal_authorised', 1)->orderBy('expires_in')->get();

        if($checks->count() == 0) return 'None Found';

        $nextCheckToExpire = $checks->first();

        if(!$nextCheckToExpire->Definition()->first()) return 'None Found';

        return $nextCheckToExpire->Definition()->first()->name . ' expires in ' . $nextCheckToExpire->expires_in . ' days';
    }


}
