<?php namespace App;

use App\Traits\RemoteConnection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GUID;

class Approval extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $guarded = [];

    use GUID, RemoteConnection;

    public function Checks()
    {
        return $this->hasOne('App\Check');
    }

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}


