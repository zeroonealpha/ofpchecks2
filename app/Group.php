<?php namespace App;

use App\Traits\GUID;
use App\Traits\RemoteConnection;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use GUID, RemoteConnection;

    public $incrementing = false;
    protected $guarded = [];

    public function Crew()
    {
        return $this->belongsToMany('App\Crew', 'crew_group', 'group_id', 'crew_id');
    }

    public function CheckGroup()
    {
        return $this->belongsToMany('App\CheckGroup');
    }

    public static function nameForId($id)
    {
        $f =  static::find($id);

        if($f) return $f->name; else return 'not found';
    }

}

