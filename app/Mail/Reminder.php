<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reminder extends Mailable
{
    use Queueable, SerializesModels;


    public $user;
    public $checks;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$checks)
    {
        $this->user = $user;
        $this->checks = $checks;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@offshoreflightplan.com', 'OFP Checks')
            ->subject('Checks & Recency Items Reminder')
            ->view('email.reminder');
    }
}
