<?php

namespace App\Console\Commands;

use Cache;
use App\Crew;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Repositories\CheckRepository;

class updateAllChecks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateAllChecks';


    protected $repository;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate all check groups for users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CheckRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }


    public function handle(CheckRepository $repository)
    {
        $users = new Crew();

        // WestStar
        $this->updateOnConnection('Weststar Sdn Bhd', $users, Carbon::now('Asia/Kuala_Lumpur'));

        // BSP
        $this->updateOnConnection('BSP', $users, Carbon::now('Asia/Kuala_Lumpur'));
    }

    private function updateOnConnection($connection, $usermodel, $date)
    {
        $usermodel->setConnection($connection);
        $crew = $usermodel->where('Deleted', 'No')->get();

        cache(['remote_connection' => $connection], 20);
        $this->repository->calculateAllChecksForUsers($crew, $date);

        Cache::forget($connection);
    }

}
