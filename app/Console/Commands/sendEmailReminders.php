<?php

namespace App\Console\Commands;

use Log;
use Mail;
use App\Crew;
use Carbon\Carbon;
use App\Mail\Reminder;
use Illuminate\Console\Command;
use App\Repositories\CheckRepository;

/**
 * Class SendEmailReminders
 * @package App\Console\Commands
 */
class sendEmailReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendEmailReminders {--test}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks all active operators for checks approaching expiry and sends emails';

    protected $email;

    /**
     * SendEmailReminders constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CheckRepository $repository
     */
    public function handle(CheckRepository $repository)
    {
        Log::info('Sending Email Reminders....');
        // $this->sendEmailsToBSP($repository);
        $this->sendEmailsToWeststar($repository);
    }

    private function sendEmailsToBSP($repository)
    {
        // Set DB Connection
        $crew = new Crew();
        $crew->setConnection('BSP');

        $users = $crew->where('Deleted', 'No')->get();

        // Calculate all checks
        cache(['remote_connection' => 'BSP'], 20);
        $repository->calculateAllChecksForUsers($users);

        foreach($users as $user)
        {
            // Get all checks from the check groups ...
            $checks = $repository->getAllChecksForUser($user);

            // Find any of these expiring within 30, 7 or 1 day
            $checks = $checks->filter(function($check) {
                if($check->Definition()->count() > 0) {
                    return (($check->latest == 1) && (($check->expires_in == 90) || ($check->expires_in ==  60) || ($check->expires_in == 30) || ($check->expires_in == 1) || ($check->expires_in == 0)));
                }
                return false;
            });

            $this->info($user->SurnamePlus  . ' has ' . $checks->count() . ' checks and recency items approaching expiry');

            // Send email if items require attention
            if(($checks->count() > 0)) {

                if($this->option('test')) $this->email = env('MAIL_USERNAME'); else $this->email = $user->email;

                $this->info('..sending email to ' . $this->email);

                Mail::to($this->email)
                    ->cc(['A.Beattie@shell.com','Saiful.roslee@shell.com', 'A.Zai@shell.com'])
                    ->bcc(env('MAIL_USERNAME'))
                    ->queue(new Reminder($user, $checks));

                Log::info('..sending email to ' . $this->email);

                // update table
                foreach($checks as $check) {
                    if($check->reminder_status == 1) $check->reminder_status = 0;
                    if($check->reminder_status == 7) $check->reminder_status = 1;
                    if($check->reminder_status == 30) $check->reminder_status = 7;
                    if($check->reminder_status == 60) $check->reminder_status = 30;
                    $check->reminder_sent = Carbon::now();
                    $check->save();
                }
            }
        }

    }

    private function sendEmailsToWeststar($repository)
    {
        // Set DB Connection
        $crew = new Crew();
        $crew->setConnection('Weststar Sdn Bhd');

        $users = $crew->where('Deleted', 'No')->get();

        // Calculate all checks
        cache(['remote_connection' => 'Weststar Sdn Bhd'], 20);
        $repository->calculateAllChecksForUsers($users);

        foreach($users as $user)
        {
            if($user->BaseICAO == 'FGSL') continue;
            if($user->BaseICAO == 'FOOG') continue;

            // Get all checks from the check groups ...
            $checks = $repository->getAllChecksForUser($user);

            // Find any of these expiring within 30, 7 or 1 day
            $checks = $checks->filter(function($check) {
                if($check->Definition()->count() > 0) {
                    return (($check->latest == 1) && (($check->expires_in == 90) || ($check->expires_in ==  60) || ($check->expires_in == 30) || ($check->expires_in == 1) || ($check->expires_in == 0)));
                }
                return false;
            });

            $this->info($user->SurnamePlus  . ' has ' . $checks->count() . ' checks and recency items approaching expiry');

            // Send email if items require attention
            if(($checks->count() > 0)) {

                if($this->option('test')) $this->email = env('MAIL_USERNAME'); else $this->email = $user->email;

                $this->info('..sending email to ' . $this->email);

                switch($user->BaseICAO)
                {
                    // Subang == HQ
                    case 'WMSA':
                        $cc_list = ['jonurhairulnizam.jusoh@weststar-aviation.aero','norashikin.samsudin@weststar-aviation.aero'];
                        break;

                    // Kerteh - KTE
                    case 'WMKE':
                        $cc_list = ['zamzuri.zakaria@weststar-aviation.aero','noorul.asyikin@weststar-aviation.aero'];
                        break;

                    // Kota Bharu - KBR
                    case 'WMKC':
                        $cc_list = ['jonurhairulnizam.jusoh@weststar-aviation.aero','norsyazwani.tajudin@weststar-aviation.aero'];
                        break;

                    // Kota Kinabalu - BKI
                    case 'WBKK':
                        $cc_list = ['alecca.alaysius@weststar-aviation.aero','fazle.yusof@weststar-aviation.aero'];
                        break;

                    // MYY - Miri
                    case 'WBGR':
                        $cc_list = ['harith.aziz@weststar-aviation.aero'];
                        break;

                    case 'GQNN':
                        $cc_list = ['norashikin.samsudin@weststar-aviation.aero','zawani.nawi@weststar-aviation.aero','norsyazwani.tajudin@weststar-aviation.aero','nkc_bm@weststar-aviation.aero','roony.koh@weststar-aviation.aero','selves@weststar-aviation.aero'];
                        break;

                    case 'VTSH':
                        $cc_list = ['sklflops@uoathai.com','norashikin.samsudin@weststar-aviation.aero','banta.abd@weststar-aviation.aero'];
                        break;

                    case 'WADD':
                        $cc_list = ['norashikin.samsudin@weststar-aviation.aero'];
                        break;

                    case 'WALL':
                        $cc_list = ['norashikin.samsudin@weststar-aviation.aero'];
                        break;

                    case 'WIHH':
                        $cc_list = ['norashikin.samsudin@weststar-aviation.aero'];
                        break;

                    case 'WION':
                        $cc_list = ['norashikin.samsudin@weststar-aviation.aero'];
                        break;
                    default:
                        $cc_list = ['norashikin.samsudin@weststar-aviation.aero'];
                        break;
                }

                $extra = ['adityo.ari@weststar-aviation.aero','abdul.syukur@weststar-aviation.aero','noorul.asyikin@weststar-aviation.aero','norsyazwani.tajudin@weststar-aviation.aero'];

                Mail::to($this->email)
                    ->cc(array_merge($cc_list, $extra))
                    ->bcc(env('MAIL_USERNAME'))
                    ->queue(new Reminder($user, $checks));

                Log::info('..sending email to ' . $this->email);

                // update table
                foreach($checks as $check) {
                    if($check->reminder_status == 1) $check->reminder_status = 0;
                    if($check->reminder_status == 7) $check->reminder_status = 1;
                    if($check->reminder_status == 30) $check->reminder_status = 7;
                    if($check->reminder_status == 60) $check->reminder_status = 30;
                    $check->reminder_sent = Carbon::now();
                    $check->save();
                }
            }
        }
    }
}
