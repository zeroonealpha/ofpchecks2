<?php namespace App\ServiceClasses;

/**
 * Class CheckGroupStatus
 *
 * @package \app\ServiceClasses
 */
class CheckGroupStatus
{

    protected $checks_invalid = 0;
    protected $recency_items_invalid = 0;

    protected $checks_with_restrictions = 0;
    protected $recency_items_with_restrictions = 0;

    protected $group_ok = true;
    protected $group_no_restrictions = true;

    protected $check_summary = '';
    protected $recency_item_summary = '';

    /*
    |--------------------------------------------------------------------------
    | SETTERS
    |--------------------------------------------------------------------------
    |
    |
    */
    public function hasInvalidCheck($name)
    {
        $this->group_ok = false;
        $this->checks_invalid ++;
        $this->check_summary .= $name . ' has EXPIRED. ';
    }

    public function hasInvalidRecencyItem($name)
    {
        $this->group_ok = false;
        $this->recency_items_invalid ++;
        $this->recency_item_summary .= $name . ' is NOT CURRENT. ';
    }

    public function hasCheckWithRestriction()
    {
        $this->group_no_restrictions = false;
        $this->checks_with_restrictions ++;
    }

    public function hasRecencyItemWithRestriction()
    {
        $this->group_no_restrictions = false;
        $this->recency_items_with_restrictions ++;
    }

    /*
    |--------------------------------------------------------------------------
    | GETTERS
    |--------------------------------------------------------------------------
    |
    |
    */
    public function hasWarnings()
    {
        return $this->group_no_restrictions;
    }

    public function numberOfChecksInvalid()
    {
        return $this->checks_invalid;
    }

    public function numberOfRecencyItemsInvalid()
    {
        return $this->recency_items_invalid;
    }

    public function numberOfChecksWithRestrictions()
    {
        return $this->checks_with_restrictions;
    }

    public function numberOfRecencyItemsWithRestrictions()
    {
        return $this->recency_items_with_restrictions;
    }

    public function checkSummary()
    {
        return $this->check_summary;
    }

    public function recencyItemSummary()
    {
        return $this->recency_item_summary;
    }

    public function getStatus()
    {
        if($this->group_ok && $this->group_no_restrictions) return GROUP_VALID;

        if($this->group_ok && !$this->group_no_restrictions) return GROUP_HAS_RESTRICTIONS;

        return GROUP_INVALID;
    }


}
