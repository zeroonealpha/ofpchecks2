<?php namespace App\ServiceClasses;


use App\Scan;
use App\Jobs\S3Upload;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Created by PhpStorm.
 * User: craigwebster
 * Date: 10/12/2015
 * Time: 14:04
 */
class Uploader
{
    protected $request;
    protected $model;

    /**
     * Uploader constructor.
     * @param $request
     * @param $model
     */
    public function __construct($request, $model)
    {
        $this->request = $request;
        $this->model = $model;
    }

    public function response()
    {
        // Check ajax request from DropZone.js and MimeType acceptable
        if ($this->request->ajax() && $this->request->hasFile('upload')) {

            // Put document in /storage/tmp/{ random(32) }.ext
            foreach($this->request->file('upload') as $file)
            {
                // Generate random filename
                $filename = str_random(32) . '.' . $file->getClientOriginalExtension();

                // Create scan database entry linked to opscom
                Scan::create([
                    'filename' => $filename,
                    'original_filename' => $file->getClientOriginalName(),
                    'scanable_id' => $this->request->input('guid'),
                    'scanable_type' => $this->model,
                    'mimetype' => $file->getMimeType()
                ]);

                // Temporarily store to disk
                $file->move(storage_path() . '/tmp/', $filename);

                // Queue the S3 Upload to speed up the user experience
                dispatch(new S3Upload($filename));
            }

            // Store success to the session
            // [revalidateform guid => true for upload ]
            session([$this->request->input('guid') => true]);

            return response()->json(['success' => 'File uploaded ok'], 200);
        }
        // Otherwise an error
        return response()->json(['error' => 'File size too large, please try a smaller one!'], 400);
    }

}

