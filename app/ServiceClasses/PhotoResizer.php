<?php namespace App\ServiceClasses;

use Image;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile as File;

/**
 * Class PhotoResizer
 * @package App\Classes
 */
class PhotoResizer {

    protected $file;
    protected $filename;
    protected $mimeType;
    protected $path;
    protected $greyscale;

    /**
     * @param File $file
     * @param array $options
     * path is re
     */
    function __construct(File $file, $options = ['path' => '/img/uploads/', 'greyscale' => false])
    {
        $this->file = $file;
        $this->path = $options['path'];
        $this->greyscale = $options['greyscale'];
    }

    public function resizeAndStore()
    {
        // Make the image
        $image = Image::make($this->file->getRealPath());

        $this->filename = str_random(16) . '.' . $this->file->getClientOriginalExtension();

        $this->mimeType = $image->mime();

        // Convert to B&W if requested !
        if ($this->greyscale) $image->greyscale();

        // Resize to 400 width and store in uploads folder
        $image = $this->resize($image, null, 500);
        $image->save(public_path($this->path . $this->filename));

        // Resize to 180x180 cropped for a thumbnail
        $image = $this->resizeForThumb($image);
        $image->save(public_path($this->path . 'thumbs/' . $this->filename));
    }

    /**
     *
     */
    public function resizeAndStoreToModel(Model $model)
    {
        $this->resizeAndStore();

        $model->Scans()->create([
            'original_filename' => $this->getFilenameWithPath(),
            'filename' => $this->getThumbFilenameWithPath(),
            'mimetype' => $this->getMimeType(),
            'uploaded_ok' => true
        ]);
    }

    public function resizeAndUpdateModel(Model $model)
    {
        if($model->Scans()->count() == 0) $this->resizeAndStoreToModel($model);

        $this->resizeAndStore();

        $model->Scans()->first()->update([
            'original_filename' => $this->getFilenameWithPath(),
            'filename' => $this->getThumbFilenameWithPath(),
            'mimetype' => $this->getMimeType(),
            'uploaded_ok' => true
        ]);
    }

    /**
     * @param $image
     * @param $width
     * @param $height
     * @return mixed
     */
    private function resize($image, $width, $height)
    {
        // Constrain aspect ratio
        $image->resize($width, $height, function($constraint) {
            $constraint->aspectRatio();
        });
        return $image;
    }

    /**
     * @param $image
     * @return mixed
     */
    private function resizeForThumb($image)
    {
        $image->fit(180,180);
        return $image;
    }

    /*
    |-------------------------------------------------------------------------
    | Getter methods
    |-------------------------------------------------------------------------
    |
    |   Storage Filenames, and mimetypes accessible
    */
    public function getRelativeFilename()
    {
        return $this->filename;
    }

    public function getFilenameWithPath()
    {
        return '/' . $this->path . $this->filename;
    }

    public function getThumbFilenameWithPath()
    {
        return '/' .$this->path . 'thumbs/' . $this->filename;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }


}
