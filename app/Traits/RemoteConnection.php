<?php namespace App\Traits;

trait RemoteConnection {

    public function __construct(array $attributes = [])
    {
        if(session('remote_connection')) {
            $this->connection = session('remote_connection');
        }
        else
        {
            $this->connection = cache('remote_connection');
        }

        parent::__construct($attributes);
    }

}

