<?php namespace App\Traits;

trait GUID {

    public function save(array $options = [])
    {
        // PHP 7 null coalesce operator
        $this->id = $this->getAttribute('id') ?? getGUID();

        parent::save($options);
    }

}
