<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CrewGroupUpdated
{
    use SerializesModels;

    public $check_group_id;
    public $crew_group_id;
    public $action;

    /**
     * CrewGroupUpdated constructor.
     * @param $check_group_id
     * @param $crew_group_id
     * @param $action
     */
    public function __construct($check_group_id, $crew_group_id, $action)
    {
        $this->check_group_id = $check_group_id;
        $this->crew_group_id = $crew_group_id;
        $this->action = $action;
    }


    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('private-channel');
    }
}
