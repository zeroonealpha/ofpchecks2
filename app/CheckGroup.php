<?php namespace App;

use App\Traits\RemoteConnection;
use Illuminate\Database\Eloquent\Model;

use DB;
use Auth;
use App\Check;
use App\Traits\GUID;

class CheckGroup extends Model
{
    use GUID, RemoteConnection;

    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $table = 'check_groups';
    protected $guarded = [];

    public function Crew()
    {
        return $this->belongsToMany('App\Crew', 'check_groups_users', 'group_id', 'user_id')->withPivot('group_name', 'group_valid', 'check_messages', 'check_summary', 'recency_messages', 'recency_item_summary');
    }

    public static function forOperator()
    {
        return static::orderBy('display_order')->with('CheckDefinitions')->get();
    }

    public function Groups()
    {
        return $this->belongsToMany('App\Group');
    }

    public function CrewGroupsAppliedTo()
    {
        return $this->Groups()->get();
    }

    public function CheckDefinitions()
    {
        return $this->belongsToMany('App\CheckDefinition', 'check_definitions_check_groups', 'group_id', 'check_def_id');
    }

    // A Check Group can extend a Check Group, so a Pivot table for itself !
    public function Extensions()
    {
        return $this->belongsToMany('App\CheckGroup', 'check_groups_check_groups', 'group_id', 'extends_group_id');
    }

    public function GroupExtendedId()
    {
        if($this->Extensions()->first()) {
            return $this->Extensions()->first()->id;
        }

        return null;
    }

    public function GroupExtendedAsListItem()
    {
        $output = "<ul>";

        foreach($this->Extensions()->get() as $ext)
        {
            $output .= "<li>" . $ext->name . "</li>";
        }
        return $output . "</ul>";
    }

    public function message()
    {
        switch ($this->pivot->group_valid) {
            case 'valid':
                return "<span class='green'><i class='fa fa-check'></i> [" . $this->pivot->check_messages . "]</span>";
                break;

            case 'invalid':
                return "<span class='red'><i class='fa fa-remove'></i> [" . $this->pivot->check_messages . "]";
                break;

            case 'valid with restrictions':
                return "<span class='orange'><i class='fa fa-exclamation'></i> [" . $this->pivot->check_messages . "]";
                break;
        }
    }

    public function status()
    {
        switch ($this->pivot->group_valid) {
            case 'valid':
                return VALID;
                break;

            case 'invalid':
                return INVALID;
                break;

            case 'valid with restrictions':
                return VALID_WITH_RESTRICTION;
                break;
        }
    }

    public function simpleStatus()
    {
        switch($this->pivot->group_valid)
        {
            case 'valid':
                return "<i class='green fa fa-check'></i>";
                break;

            case 'invalid':
                return "<i class='red fa fa-remove'></i>";
                break;

            case 'valid with restrictions':
                return "<i class='orange fa fa-exclamation'></i>";
                break;
        }
    }

    public function numberOfChecks()
    {
        return $this->CheckDefinitions()->count();
    }
}

