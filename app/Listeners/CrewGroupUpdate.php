<?php

namespace App\Listeners;

use App\Group;
use App\CheckGroup;
use App\Events\CrewGroupUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrewGroupUpdate
{

    protected $errors;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->errors = 0;
    }

    /**
     * Handle the event.
     *
     * @param  CrewGroupUpdated  $event
     * @return void
     */
    public function handle(CrewGroupUpdated $event)
    {
        // Find the group updated, or added/removed from check group
        $CrewGroup = Group::find($event->crew_group_id);

        // Event applies to just one Check Group
        if($event->check_group_id)
        {
            $CheckGroup = CheckGroup::find($event->check_group_id);

            // Store
            try {
                switch($event->action)
                {
                    case(ATTACHED):
                        $CheckGroup->Groups()->attach($CrewGroup);
                        flash('Check Group Applied!', $CheckGroup->name . " added to Crew Group " . $CrewGroup->name, 'success');
                        break;
                    case(DETACHED):
                        $CheckGroup->Groups()->detach($CrewGroup);
                        flash('Crew Group Removed!',  $CrewGroup->name . " removed from " . $CheckGroup->name, 'warning');
                        break;
                }

                $this->updateCheckGroup($CheckGroup);

                // On error, query exception thrown
            } catch (QueryException $e) {
                $this->errors++;
            }
        }
        else
        {
            $CheckGroups = CheckGroup::forOperator();

            foreach($CheckGroups as $CheckGroup)
            {
                $this->updateCheckGroup($CheckGroup);
            }

            flash('Crew Group Updated!',  $CrewGroup->name . " has been updated", 'success');

        }
    }

    private function updateCheckGroup($CheckGroup)
    {
        $allMembers = collect();

        foreach($CheckGroup->Groups()->get() as $group)
        {
            $members = $group->Crew()->get();
            $allMembers = $allMembers->merge($members);
        }

        $userIDs = $allMembers->map(function($member) { return $member->UserID; });

        $CheckGroup->Crew()->sync($userIDs->toArray());  // Adds / Removes users associated with this group
    }
}


