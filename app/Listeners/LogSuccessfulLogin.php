<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;

class LogSuccessfulLogin
{
    /**
     * LogSuccessfulLogin constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param Login $login
     */
    public function handle(Login $login)
    {
    }
}
