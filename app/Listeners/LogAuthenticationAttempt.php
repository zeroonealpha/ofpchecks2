<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Attempting;


class LogAuthenticationAttempt
{

    /**
     * LogAuthenticationAttempt constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Attempting  $event
     * @return void
     */
    public function handle(Attempting $event)
    {
        session(['remote_connection' => $event->credentials['company']]);
    }
}
