<?php namespace App\Repositories;

use App\Check;
use App\Crew;
use Carbon\Carbon;
use App\CheckGroup;
use App\CheckDefinition;
use Illuminate\Http\Request;
use App\ServiceClasses\CheckGroupStatus;

/**
 * Class CheckRepository
 * @package ZeroOneAlpha\Checkify
 */
class CheckRepository {

    // Implements all models involved with checks into one place
    // Deals with interactions between Checks and Definitions
    protected $checks;
    protected $checkGroups;
    protected $checkDefinitions;

    /**
     * @param \App\Check $checkModel
     * @param \App\CheckDefinition $checkDefinitionModel
     * @param \App\CheckGroup $checkGroupModel
     */
    public function __construct(Check $checkModel, CheckDefinition $checkDefinitionModel, CheckGroup $checkGroupModel)
    {
        // Four Eloquent Models Make up this repository.
        $this->checks = $checkModel;
        $this->checkGroups = $checkGroupModel;
        $this->checkDefinitions = $checkDefinitionModel;
    }

    public function setConnection($conn)
    {
        $this->checks->setConnection($conn);
        $this->checkGroups->setConnection($conn);
        $this->checkDefinitions->setConnection($conn);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function allChecksForUser(string $userId)
    {
        return $this->checks->where('user_id', '=',  $userId)->with('Definition')->get();
    }

    public function calculateAllChecksForUsers($users, Carbon $date = null)
    {
        foreach($users as $user)
        {
            $this->calculateCheckGroupForUser($user, $date);
        }
    }

    public function getAllChecksForUser($user)
    {
        $groups = $user->CheckGroups()->get();

        $checks = collect();

        foreach ($groups as $group) {
            foreach ($group->CheckDefinitions()->get() as $definition) {
                // Get latest check and previous
                $check = $definition->LatestForUser($user);
                if($check) $checks->push($check);
            }
        }

        return $checks;
    }

    public function getAllRecencyItemsForUser($user)
    {
        $groups = $user->CheckGroups()->get();

        $items = collect();

        foreach ($groups as $group) {
            foreach ($group->RecencyItemDefinitions()->get() as $definition) {
                // Get latest check and previous
                $item = $definition->LatestForUser($user);
                if($item) $items->push($item);
            }
        }

        return $items;
    }

    public function calculateCheckGroupForUser(Crew $user, Carbon $date = null)
    {
        if($date == null) $date = Carbon::now();

        // For each of the user groups
        foreach($user->checkGroups()->get() as $group)
        {
            $group->pivot->group_name = $group->name;

            $CheckGroupState = $this->returnCheckGroupStatus($group, $user, $date);

            // If all valid, then the group is valid
            switch($CheckGroupState->getStatus())
            {
                case (GROUP_VALID): $this->markGroupAsValid($group);
                    break;
                case (GROUP_HAS_RESTRICTIONS):  $this->markGroupWithRestrictions($group, $CheckGroupState);
                    break;
                case (GROUP_INVALID): $this->markGroupAsInvalid($group, $CheckGroupState);
                    break;
            }
        }

        // Now they are all calculated individually,
        // We need to check if the extended groups are also valid
        // If not, then the parent groups become invalidated..
        foreach($user->checkGroups()->get() as $group)
        {
            if(! $this->extendedGroupIsValid($group, $user)) $this->markGroupAsInvalidDueToExtendedGroupInvalid($group);
        }
    }

    public function returnCheckGroupStatus(CheckGroup $group, Crew $user, Carbon $date = null)
    {
        if($date == null) $date = Carbon::now();

        $groupStatus = new CheckGroupStatus();

        foreach($group->CheckDefinitions()->get() as $definition)
        {
            // Get latest check and previous
            $check = $definition->LatestForUser($user);

            // No item recorded, so INVALID if no restriction applicable
            if(! $check) {
                ($definition->generate_restriction_on_expiry) ? $groupStatus->hasCheckWithRestriction() : $groupStatus->hasInvalidCheck($definition->name);
                continue;
            }

            // Determine if each one is valid (supply previous check to see if it cover the new one)
            switch($check->getStatus($date))
            {
                case (VALID_PENDING_APPROVAL): $groupStatus->hasInvalidCheck($definition->name);
                    break;
                case (VALID_WITH_RESTRICTION): $groupStatus->hasCheckWithRestriction();
                    break;
                case (INVALID): $groupStatus->hasInvalidCheck($definition->name);
                    break;
            }
        }

        return $groupStatus;
    }

    /**
     * @param $userId
     * @param $days
     * @return mixed
     *
     * Returns a collection of all checks for a User expiring
     * within a specified number of days.
     */
    public function approachingExpiry($userId , $days)
    {
        return $this->checks->where('user_id', $userId)->where('expiry_date', '<=', Carbon::now()->addDays($days))->get();
    }

    /**
     * @array $attributes
     *
     * Adds a check record for a User, for a specific check definition by id.
     */
    public function addCheck($id, $check)
    {
        // Retrieve check definition
        $this->checkDefinitions->find($id)->checks()->save($check);
    }

    /*
    |--------------------------------------------------------------------------
    | DEFINITIONS
    |--------------------------------------------------------------------------
    |
    |
    */
    public function createDefinition(Request $request)
    {
        // Add operator id to the request
        $values = $request->all();

        $values['operator_id'] = session('operator.id');

        $this->checkDefinitions->create($values);
    }


    public function allDefinitions()
    {
        return $this->checkDefinitions->where('operator_id', session('operator.id'))->get();
    }

    /*
    |--------------------------------------------------------------------------
    | GROUPS
    |--------------------------------------------------------------------------
    |
    |
    */
    public function findGroup($id)
    {
        return $this->checkGroups->with('CheckDefinitions')->find($id);
    }

    public function allGroups()
    {
        return $this->checkGroups->where('operator_id', session('operator.id'))->with('CheckDefinitions')->orderBy('display_order')->get();
    }

    public function allGroupsExcept($id)
    {
        return $this->checkGroups->where('operator_id', session('operator.id'))->where('id', '<>', $id)->orderBy('display_order')->get();
    }

    public function createCheckGroup(Request $request)
    {
        // Add operator id to the request
        $values = $request->all();

        $values['operator_id'] = session('operator.id');

        $this->checkGroups->create($values);
    }

    public function addCheckDefinitionIdToGroupId($checkDefinition_id, $group_id)
    {
        $this->checkGroups->find($group_id)->CheckDefinitions()->attach($checkDefinition_id);
    }

    public function deleteCheckDefinitionIdFromGroupId($checkDefinition_id, $group_id)
    {
        if($group_id)
        {
            $this->checkGroups->find($group_id)->CheckDefinitions()->detach($checkDefinition_id);
        } else {
            $this->checkDefinitions->destroy($checkDefinition_id);
        }
    }

    public function attachExtensionToGroupId($group_id, $group_to_extend)
    {
        $currentExtensionId = $this->findGroup($group_id)->groupExtendedId();

        if($group_to_extend != $currentExtensionId)
        {
            $this->checkGroups->find($group_id)->Extensions()->attach($group_to_extend);
        }
    }

    public function detachExtensionFromGroupId($group_id, $group_to_delete)
    {
        $currentExtensionId = $this->findGroup($group_id)->groupExtendedId();

        if ($group_to_delete == $currentExtensionId) {
            $this->checkGroups->find($group_id)->Extensions()->detach($group_to_delete);
        }
    }

    private function markGroupAsValid($group)
    {
        $group->pivot->group_valid = 'valid';
        $group->pivot->check_messages = 'Checks ALL OK';
        $group->pivot->save();
    }

    private function markGroupWithRestrictions($group, CheckGroupStatus $groupStatus)
    {
        $group->pivot->group_valid = 'valid with restrictions';
        $group->pivot->check_messages = $groupStatus->numberOfChecksWithRestrictions() . ' Check(s) Restricted';
        $group->pivot->check_summary = $groupStatus->checkSummary();
        $group->pivot->save();
    }

    private function markGroupAsInvalid($group, CheckGroupStatus $groupStatus)
    {
        $group->pivot->group_valid = 'invalid';
        $group->pivot->check_messages = $groupStatus->numberOfChecksInvalid() . ' Check(s) invalid';
        $group->pivot->check_summary = $groupStatus->checkSummary();
        $group->pivot->save();
    }

    private function markGroupAsInvalidDueToExtendedGroupInvalid($group)
    {
        $group->pivot->group_valid = 'invalid';
        $group->pivot->check_messages = 'Base Group Invalid';
        $group->pivot->save();
    }

    private function extendedGroupIsValid($group, $user)
    {
        $id = $group->GroupExtendedId();

        if($id)
        {
            $g = $user->checkGroups()->where('id', $id)->first();
            if($g) {
                if ($g->pivot->group_valid != 'invalid') return true; else return false;
            }
        }

        // Otherwise there isn't, so this rule doesn't apply..
        return true;
    }
}
