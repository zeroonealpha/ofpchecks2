<?php namespace App;

// This will be an Eloquent Model

use App\Traits\RemoteConnection;
use DB;
use Auth;
//use App\Type;
//use App\Check;
use App\Traits\GUID;
use Illuminate\Database\Eloquent\Model;

class CheckDefinition extends Model
{
    use GUID, RemoteConnection;

    public $incrementing = false;
    protected $table = 'check_definitions';
    protected $guarded = [];

    public static function allForOperator()
    {
        return static::all();
    }

    public function Checks()
    {
        return $this->hasMany('App\Check','check_def_id','id');
    }

//    public function CheckGroups()
//    {
//        return $this->belongsToMany('App\CheckGroup', 'check_definitions_check_groups', 'check_def_id','group_id');
//    }
//
//    public function Type()
//    {
//        return $this->belongsTo('App\Type', 'type_id', 'id');
//    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    |
    */

    public function LatestForUser($user)
    {
        $check = $this->Checks()->where('user_id', $user->UserID)->orderBy('date_performed', 'DESC')->orderBy('created_at','DESC')->first();

        if($check){
            $check->update(['latest' => 1]);

            DB::connection(session('remote_connection'))
                    ->table('checks')
                    ->where('user_id', $user->UserID)
                    ->where('check_def_id', $check->check_def_id)
                    ->where('id', '<>', $check->id)
                    ->update(['latest' => 0]);
        }

        return $check;
    }

    public function LatestForUserWithoutUpdate($user)
    {
        return $this->Checks()->where('user_id', $user->UserID)->where('latest', 1)->first();
    }


    public function typeAsString()
    {
        if ($this->type_specific) {
            return $this->type_id;
        }

        return 'ANY TYPE';
    }

}


