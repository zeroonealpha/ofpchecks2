<?php namespace App;

use App\Traits\RemoteConnection;
Use DB;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use RemoteConnection;

    public $incrementing = false;
    protected $table = 'aircraftlist';


    public static function allForOperator()
    {
        return static::select('Type','ICAO_Type')->where('Deleted', 'No')->distinct()->get();
    }
}

