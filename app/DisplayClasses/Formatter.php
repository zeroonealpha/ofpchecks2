<?php
/**
 * Created by PhpStorm.
 * User: craigwebster
 * Date: 13/10/15
 * Time: 13:23
 */

namespace App\DisplayClasses;


interface Formatter
{
    // PHP7 return type
    public function output(): string;
}
