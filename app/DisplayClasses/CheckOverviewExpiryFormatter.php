<?php namespace App\DisplayClasses;

/**
 * Created by PhpStorm.
 * User: craigwebster
 * Date: 13/10/15
 * Time: 13:11
 */

use Carbon\Carbon;

class CheckOverviewExpiryFormatter implements Formatter
{

    protected $daysToExpiry;
    protected $expiry_date;
    /**
     * CheckExpiryFormatter constructor.
     * @param Carbon $expiry_date
     * @param $daysToExpiry
     */
    public function __construct($expiry_date, $daysToExpiry)
    {
        $this->daysToExpiry = $daysToExpiry;
        $this->expiry_date = Carbon::parse($expiry_date)->format('d M Y');
    }

    public function output(): string
    {
        if($this->daysToExpiry >= 60) return ("<strong style='background-color: #23d404; color: white; padding: 4px;'>" . $this->expiry_date . "</strong>");
        if($this->daysToExpiry >= 30) return ("<strong style='background-color: blue; color: white; padding: 4px;'>" . $this->expiry_date . "</strong>");
        if($this->daysToExpiry > 1) return ("<strong style='background-color: #f6fc35; padding: 4px;'>" . $this->expiry_date . "</strong>");
        if($this->daysToExpiry == 1) return ("<strong style='background-color: red; color: white; padding: 4px;'>Expires Tomorrow!</strong>");
        if($this->daysToExpiry == 0) return ("<strong style='background-color: red; color: white; padding: 4px;'>Expires Today!</strong>");
        if($this->daysToExpiry == -1) return ("<strong style='background-color: red; color: white; padding: 4px;'>EXPIRED Yesterday!</strong>");

        return "<strong style='background-color: red; color: white; padding: 4px;'>EXPIRED on " . $this->expiry_date . "</strong>";
    }
}




