<?php namespace App\DisplayClasses;

/**
 * Created by PhpStorm.
 * User: craigwebster
 * Date: 13/10/15
 * Time: 13:11
 */

use Carbon\Carbon;

class CheckExpiryFormatter implements Formatter
{

    protected $daysToExpiry;
    protected $expiry_date;
    /**
     * CheckExpiryFormatter constructor.
     * @param Carbon $expiry_date
     * @param $daysToExpiry
     */
    public function __construct($expiry_date, $daysToExpiry)
    {
        $this->daysToExpiry = $daysToExpiry;
        $this->expiry_date = Carbon::parse($expiry_date);
    }

    public function output(): string
    {
        if($this->daysToExpiry >= 90) return ("<strong>Expires in " . $this->daysToExpiry . " days</strong> on " . $this->expiry_date->format('D jS M Y'));
        if($this->daysToExpiry >= 60) return ("<strong class='orange'>Expires in " . $this->daysToExpiry . " days</strong> on " . $this->expiry_date->format('D jS M Y'));
        if($this->daysToExpiry >= 30) return ("<strong class='orange'>Expires in " . $this->daysToExpiry . " days</strong> on " . $this->expiry_date->format('D jS M Y'));
        if($this->daysToExpiry > 1) return ("<strong class='red'>Expires in " . $this->daysToExpiry . " days</strong> on " . $this->expiry_date->format('D jS M Y'));
        if($this->daysToExpiry == 1) return ("<strong class='red'>Expires Tomorrow! (" . $this->expiry_date->format('D jS M Y') . ")</strong>");
        if($this->daysToExpiry == 0) return ("<strong class='red'>Expires Today!</strong>");
        if($this->daysToExpiry == -1) return ("<strong class='red'>EXPIRED Yesterday!</strong>");
        if($this->daysToExpiry < -10000) return "OK";

        return "<strong class='red'>EXPIRED " . abs($this->daysToExpiry) . " days ago</strong>";
    }
}
