<?php
/**
 * Created by PhpStorm.
 * User: craigwebster
 * Date: 08/12/2015
 * Time: 09:20
 */

namespace App\DisplayClasses;

class FlashMessage
{
    /**
     * FlashMessage
     */
    public function message($title, $message, $type)
    {
        session()->flash('flash_message', [
            'title' => $title,
            'message' => $message,
            'type' => $type
        ]);

    }

}

