<?php namespace App\Http\Controllers;

use App\Crew;
use Auth;
use App\Check;
use App\Http\Requests;
use App\CheckDefinition;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\CheckApproved;
use App\ServiceClasses\Uploader;

class CheckController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function revalidate($definitionId, $userid)
    {
        $definition = CheckDefinition::where('id', $definitionId)->firstOrFail();

        return view('checks.revalidate.index')->withUserId($userid)->withDefinition($definition)->withGuid(getGUID());
    }

    public function revalidateStore(Request $request)
    {

        // Validate the request
        $this->validate($request, [
            'date_performed' => 'date_format:d/m/Y'
        ]);

        // Store a new check
        $check = $this->createCheck($request);

        $crew = $check->Crew()->first();

        // Is a scan required?
        if($check->scanRequired())
        {
            // Was there a document attached?
            if (session($request->input('guid')) == false)
            {
                // Show flash message
                flash('Did you forget something?', 'There is no uploaded document', 'danger');

                // Delete the check !
                $check->delete();

                return redirect()->back();
            }
        }

        // If approved checkbox is checked, then approve check..
        if($request->input('check_approved')) approveCheck($check, Auth::user()->id);

        if($check->RenewalAuthorised()) {

            flash('Check Revalidated Successfully', $crew->name . ' will be notified', 'success');

            return redirect('/home/' . $crew->UserID);
        }

        flash('Check approved',  'Awaiting Approval', 'warning');

        // Redirect to Ops comm dashboard
        return redirect('/home/' . $crew->UserID);

    }

    public function edit($checkid, $crewid)
    {
        $check = Check::find($checkid);

        $definition = $check->Definition()->first();

        if(!$definition) {
            flash('Error', 'Definition not found for this check!', 'danger');

            return redirect('/home/' . $crewid);
        }

        return view('checks.revalidate.edit')->withCheck($check)->withDefinition($definition);
    }

    public function update(Request $request, $check_id, $user_id)
    {
        // Validate the request
        $this->validate($request, [
            'date_performed' => 'date_format:d/m/Y'
        ]);

        // Store a new check
        $check = Check::find($check_id);

        if(!$check) return redirect('/home');

        $this->updateCheck($request, $check);

        if($request->input('check_approved')) approveCheck($check, Auth::user()->id);

        flash('Success', 'Check has been updated', 'success');

        // Redirect to Ops comm dashboard
        return redirect('/home/' . $user_id);
    }


    public function delete($checkid, $crewid)
    {
        Check::destroy($checkid);

        flash('Check Deleted', 'Be sure to submit another check', 'danger');

        // Redirect to Ops comm dashboard
        return redirect('/home/' . $crewid);
    }

    public function approve($checkid, $crewid)
    {
        $check = Check::find($checkid);

        if(approveCheck($check, Auth::user()->id))
        {
            flash('Success', 'Check approved',  'success');
        }
        else
        {
            flash('Error', 'You have already approved this check.', 'warning' );
        }

        // Redirect to Ops comm dashboard
        return redirect('/home/' . $crewid);
    }

    private function createCheck($request)
    {
        $dp = Carbon::createFromFormat('d/m/Y', $request->input('date_performed'));
        if($request->input('expiry_date') != "0/0/0000") {
            $exp = Carbon::createFromFormat('d/m/Y', $request->input('expiry_date'));
        } else {
            $exp = "2099-01-01 00:00:00";
        }

        return Check::create([
            'id' => $request->input('guid'),
            'user_id' => $request->input('user_id'),
            'date_performed' => $dp,
            'expiry_date' => $exp,
            'check_def_id' => $request->input('definition_id'),
            'remarks' => $request->input('remarks')
        ]);
    }

    private function updateCheck($request, $check)
    {
        $dp = Carbon::createFromFormat('d/m/Y', $request->input('date_performed'));
        if($request->input('expiry_date') != "0/0/0000") {
            $exp = Carbon::createFromFormat('d/m/Y', $request->input('expiry_date'));
        } else {
            $exp = "2099-01-01 00:00:00";
        }

        $check->update([
            'date_performed' => $dp,
            'expiry_date' => $exp,
            'remarks' => $request->input('remarks')
        ]);
    }

    public function archive($defid, $crewid)
    {
        $checks = Check::where('check_def_id', $defid)->where('user_id', $crewid)->orderBy('date_performed','DESC')->orderBy('created_at','DESC')->get();

        $crew = Crew::find($crewid);

        $definition = CheckDefinition::find($defid);

        return view('checks.archive.index')->withChecks($checks)->withDefinition($definition)->with('crew_id', $crewid)->withCrew($crew);
    }
}

