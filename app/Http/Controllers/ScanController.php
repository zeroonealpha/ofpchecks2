<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceClasses\Uploader;
use App\Check;

use App\Http\Requests;

class ScanController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $uploader = new Uploader($request, Check::class);

        return $uploader->response();
    }

}
