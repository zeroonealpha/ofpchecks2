<?php

namespace App\Http\Controllers;

use Auth;
use App\Crew;
use Illuminate\Http\Request;
use App\Repositories\CheckRepository;

class DashboardController extends Controller
{

    protected $checkRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CheckRepository $checkRepository)
    {
        $this->middleware('auth');
        $this->checkRepository = $checkRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->checkRepository->setConnection(session('remote_connection'));

        $numberOfCrew = Crew::all()->count();

        if($numberOfCrew > 0) {
            $this->checkRepository->calculateCheckGroupForUser(Crew::first());

            return view('dashboard.index')->withCrews(Crew::forOperator()->get())
                                                ->withCrew(Crew::forOperator()->first());
        }

        return view('dashboard.nocrew');
    }

    public function show(Request $request)
    {
        $id = $request->get('crew');

        $this->checkRepository->setConnection(session('remote_connection'));

        $this->checkRepository->calculateCheckGroupForUser(Crew::find($id));

        return view('dashboard.index')  ->withCrews(Crew::forOperator()->get())
                                        ->withCrew(Crew::find($id));
    }

    public function showByID($userid)
    {
        $this->checkRepository->calculateCheckGroupForUser(Crew::find($userid));

        return view('dashboard.index')  ->withCrews(Crew::forOperator()->get())
                                        ->withCrew(Crew::find($userid));
    }
}
