<?php

namespace App\Http\Controllers;

use App\Crew;
use App\Group;
use App\CheckGroup;
use Illuminate\Http\Request;
use App\Repositories\CheckRepository;

class CheckOverviewController extends Controller
{
    protected $checkRepository;

    /**
     * CheckGroupController constructor.
     * @param $checkRepository
     */
    public function __construct(CheckRepository $checkRepository)
    {
        $this->middleware('auth');
        $this->checkRepository = $checkRepository;
    }

    public function index($checkgroup_id)
    {
        //$this->checkRepository->calculateAllChecksForUsers(Crew::forOperator()->get());
        $checkGroup = CheckGroup::where('id', $checkgroup_id)->orderBy('display_order')->with('CheckDefinitions')->first();
        return view('overview.index')->with('crews', Crew::forOperator()->orderBy('Rank')->get())->with('checkgroup', $checkGroup);
    }
}
