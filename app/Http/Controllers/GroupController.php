<?php namespace App\Http\Controllers;

use Auth;
use App\Crew;
use App\Group;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\CrewGroupUpdated;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $groups = Group::all();

        return view('crew.groups.index')->withGroups($groups);
    }

    public function create()
    {
        return view('crew.groups.create');
    }

    public function store(Request $request)
    {
        // Create group
        $group = Group::create($request->all());

        return redirect('/crew/groups');
    }

    public function destroy($id)
    {
        Group::destroy($id);

        flash('Deleted', 'Crew Group Deleted Successfully', 'danger');

        return redirect('/crew/groups');
    }

    public function edit($id)
    {
        $group = Group::find($id);

        $crews= Crew::ForOperator()->get();

        return view('crew.groups.edit')->withGroup($group)->withCrews($crews);
    }

    public function add(Request $request, $id)
    {
        // Find the group to add to
        $group = Group::find($id);

        // Add all the users to the groups
        foreach($request->input('crew') as $crew)
        {
            $group->Crew()->attach($crew);
        }

        event(new CrewGroupUpdated(null, $id, UPDATED));

        flash('Success!', 'User added to group', 'success');

        return redirect("/crew/groups/$id/edit");
    }

    public function remove($groupId, $userId)
    {
        $group = Group::find($groupId);

        $group->Crew()->detach($userId);

        event(new CrewGroupUpdated(null, $groupId, UPDATED));

        flash('Removed', 'User removed from group', 'warning');

        return redirect("/crew/groups/$groupId/edit");
    }

}


