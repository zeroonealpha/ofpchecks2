<?php

namespace App\Http\Controllers;

use App\Repositories\CheckRepository;
use Auth;
use App\Type;
use App\Http\Requests;
use App\CheckDefinition;
use Illuminate\Http\Request;


class CheckDefinitionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $definitions = CheckDefinition::allForOperator();

        $types = Type::allForOperator();

        return view('checks.definitions.index')->withDefinitions($definitions)->withTypes($types);
    }

    public function edit($id)
    {
        $definition = CheckDefinition::find($id);

        $types = Type::allForOperator();

        return view('checks.definitions.edit')->withDefinition($definition)->withTypes($types);
    }

    public function create()
    {
        $types = Type::allForOperator();

        return view('checks.definitions.create')->withTypes($types);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required'
        ]);

        $values = $request->all();

        $values['operator_id'] = Auth::user()->company;

        CheckDefinition::create($values);

        return redirect('/checks/definitions');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $values = $request->all();

        $values['operator_id'] = Auth::user()->company;

        $check = CheckDefinition::find($id);

        $check->update($values);

        return redirect('/checks/definitions');
    }

    public function destroy(Request $request, CheckRepository $repo)
    {
        $repo->deleteCheckDefinitionIdFromGroupId($request->input('definition_id'), null);

        flash('Success !','Check Definition Deleted.', 'danger');

        return redirect('/checks/definitions');
    }
}

