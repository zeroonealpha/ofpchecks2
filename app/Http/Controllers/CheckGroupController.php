<?php

namespace App\Http\Controllers;

use App\CheckDefinition;
use App\Group;
use App\Repositories\CheckRepository;
use Auth;
use App\CheckGroup;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\CrewGroupUpdated;

class CheckGroupController extends Controller
{

    protected $checkRepository;

    /**
     * CheckGroupController constructor.
     * @param $checkRepository
     */
    public function __construct(CheckRepository $checkRepository)
    {
        $this->middleware('auth');
        $this->checkRepository = $checkRepository;
    }

    public function index()
    {
        $groups = CheckGroup::all();

        $crewGroups = Group::all();

        return view('checks.groups.index')->withGroups($groups)->withCrewGroups($crewGroups);
    }

    public function create()
    {
        return view('checks.groups.create');
    }

    public function destroy($id)
    {
        CheckGroup::destroy($id);

        return redirect('/checks/groups');
    }

    public function store(Request $request)
    {
        // Add operator id to the request
        $values = $request->all();

        $values['operator_id'] = Auth::user()->company;

        CheckGroup::create($values);

        return redirect('/checks/groups');
    }

    public function edit($id)
    {
        $group = CheckGroup::find($id);

        $otherGroups = CheckGroup::where('id','<>', $id)->get();

        $definitions = CheckDefinition::all();

        return view('checks.groups.edit')->withGroup($group)->with('other_groups',$otherGroups)->withDefinitions($definitions);
    }

    public function addCheckDefinitionToGroup(Request $request)
    {
        $group_id = $request->input('group_id');

        $this->checkRepository->setConnection(session('remote_connection'));

        // Add these groups to the group extension table if there are some extensions selected
        if($request->input('groups')) {
            foreach($request->input('groups') as $group_id_to_extend)
            {
                $this->checkRepository->attachExtensionToGroupId($group_id, $group_id_to_extend);
            }
        }

        // Add these check definitions to the group
        if($request->input('checks')) {
            foreach ($request->input('checks') as $checkDefinition_id) {
                $this->checkRepository->addCheckDefinitionIdToGroupId($checkDefinition_id, $group_id);
            }
        }

        return redirect("/checks/groups/$group_id/edit");
    }

    public function deleteCheckDefinitionFromGroup($group_id, $check_id)
    {
        $this->checkRepository->setConnection(session('remote_connection'));

        $this->checkRepository->deleteCheckDefinitionIdFromGroupId($check_id,$group_id);

        return redirect("/checks/groups/$group_id/edit");
    }

    /*
|-------------------------------------------------------------------------
| Application of Check Groups to Users
|-------------------------------------------------------------------------
|
|   Group-> crew groups
*/

    // Cater for adding/removing users to Crew Groups, corresponding check groups must also be added/removed
    public function applyCheckGroupToCrewGroup($check_group_id, $crew_group_id)
    {
        event(new CrewGroupUpdated($check_group_id, $crew_group_id, ATTACHED));

        return redirect('/checks/groups/');
    }

    // Cater for adding/removing users to Crew Groups, corresponding check groups must also be added/removed
    public function removeCheckGroupFromCrewGroup($check_group_id, $crew_group_id)
    {
        event(new CrewGroupUpdated($check_group_id, $crew_group_id, DETACHED));

        return redirect('/checks/groups/');
    }

    public function detachExtensionFromGroup($groupid, $extensionid)
    {
        $this->checkRepository->setConnection(session('remote_connection'));

        $this->checkRepository->detachExtensionFromGroupId($groupid, $extensionid);

        return redirect('/checks/groups/' . $groupid . '/edit');
    }

}
