<?php

// Check group status
define('INVALID',  0);
define('VALID_WITH_RESTRICTION',  1);
define('VALID',  2);
define('VALID_PENDING_APPROVAL', 3);
define('PREVIOUS_CHECK_VALID_LATEST_PENDING_APPROVAL', 4);
define('VALID_FOREVER', 5);

define('APPROVALS_REQUIRED', 2);

define('GROUP_INVALID', 0);
define('GROUP_VALID', 1);
define('GROUP_HAS_RESTRICTIONS', 2);

define('ATTACHED', 1);
define('DETACHED', 2);
define('UPDATED', 3);

define('OPS_BASIC', 1);
define('OPS_ADMIN', 2);
