<?php

use Carbon\Carbon;
use App\Scan;
use Aws\S3\S3Client;
use App\Event;


// Flash Message
function flash($title, $message, $type = 'info')
{
    $flash = app('App\DisplayClasses\FlashMessage');

    $flash->message($title, $message, $type);

}

function getGUID()
{
    if (function_exists('com_create_guid')){

        return com_create_guid();

    } else {
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8).$hyphen
        .substr($charid, 8, 4).$hyphen
        .substr($charid,12, 4).$hyphen
        .substr($charid,16, 4).$hyphen
        .substr($charid,20,12);
        return $uuid;
    }
}

function DisplayOrderOptionGenerator()
{
    $html = '';

    for($i=0; $i < \App\CheckGroup::count(); $i++)
    {
        $html .= "<option value='" . $i . "'>" . $i . "</option>";
    }

    return $html;
}

function ColourOptionGenerator()
{

    $colours = ['Red', 'Green', 'Yellow', 'Blue', 'Black', 'Orange', 'Purple'];

    $html = '';

    foreach($colours as $colour)
    {
        $lower_colour = strtolower($colour);

        $html .= "<option value='$lower_colour'><i slot='span' class='$lower_colour fa fa-circle'></i> $colour </option>";
    }

    return $html;
}

function approveCheck($check, $opsid)
{
    $approvals = $check->Approvals()->where('check_id', $check->id)->where('user_id', $opsid)->get();

    // Check we haven't already approved this check !
    if(count($approvals) === 0)
    {
        $check->Approvals()->create([
            'user_id' => $opsid
        ]);

        $check->touch();

        return true;
    }

    return false;
}

function expiry($expiry_date, $daystoexpiry)
{
    $formatter = new App\DisplayClasses\CheckExpiryFormatter($expiry_date, $daystoexpiry);

    return $formatter->output();
}

function toDate($date)
{
    $date = Carbon::parse($date);

    return $date->format('d-M-Y');
}

function daysToExpiry($expiry_date = null, Carbon $date = null)
{
    $expiry_date = Carbon::parse($expiry_date);

    if($date == null) $date = Carbon::now('UTC');

    // So we don't fuck with the date object and send it back different !
    $test_date = clone($date);

    if($expiry_date == null) return -999;

    if($expiry_date->toDateString() == $test_date->addDay()->toDateString()) return 1;
    if($expiry_date->toDateString() == $test_date->subDay()->toDateString()) return 0;
    if($expiry_date->toDateString() == $test_date->subDay()->toDateString()) return -1;

    if($expiry_date->isFuture()) $test_date->addDay()->startOfDay();
    if($expiry_date->isPast()) $test_date->addDay()->endOfDay();
    $days = $test_date->diffInDays($expiry_date, false);

    // Destroy it
    unset($test_date);

    return $days;
}

function createPreSignedRequestForScan($id, $timeout = '+20 minutes')
{
    $scan = Scan::find($id);

    $s3Client = new S3Client([
        'region'  => env('S3_REGION'),
        'version' => 'latest',
    ]);

    $cmd = $s3Client->getCommand('GetObject', [
        'Bucket' => env('S3_BUCKET'),
        'Key'    => $scan->filename     // This is the key/filename of the object in the bucket
    ]);

    $request = $s3Client->createPresignedRequest($cmd, $timeout);

    // Get the actual presigned-url
    return (string) $request->getUri();
}
