<?php namespace App;

use App\Traits\GUID;
use App\Traits\RemoteConnection;
use Illuminate\Database\Eloquent\Model;

class Scan extends Model
{
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $guarded= ['created_at', 'updated_at'];

    use GUID;

    /**
     * Get all of the owning scanable models.
     */
    public function scanable()
    {
        return $this->morphTo();
    }


    public function icon()
    {
        switch($this->mimetype)
        {
            case 'image/jpeg':
                return  "<i class='fa fa-image'></i>";

            case 'application/pdf':
                return  "<i class='fa fa-file-pdf-o'></i>";

            case 'application/msword':
                return  "<i class='fa fa-file-word-o'></i>";

            case 'application/excel':
                return  "<i class='fa fa-file-excel-o'></i>";
        }

        // Default just a generic file icon
        return  "<i class='fa fa-file-archive-o'></i>";

    }
}

