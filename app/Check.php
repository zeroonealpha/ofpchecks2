<?php namespace App;

use App\DisplayClasses\CheckOverviewExpiryFormatter;
use App\Traits\RemoteConnection;
use Auth;
use App\User;
use App\CheckDefinition;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\DisplayClasses\CheckExpiryFormatter;

// This class store thes specific date information for the check
// This will be an Eloquent model
class Check extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'checks';
    //protected $dates = ['date_performed', 'expiry_date'];

    use RemoteConnection;

    /**
     * @param array $options
     * @return null
     */
    public function save(array $options = [])
    {
        // PHP 7 null coalesce operator
        $this->id = $this->getAttribute('id') ?? getGUID();

        $this->setAttribute('renewal_authorised', $this->RenewalAuthorised());

        parent::save($options);
    }

    /**
     * By overriding this method, we allow the relationship to accept a built (and modified) object. This way we can modify the table's name.
     * @param $class
     * @return mixed
     */
    protected function newRelatedInstance($class) {
        if (is_string($class))
            $class = new $class;

        return tap($class, function ($instance) {
            if (!$instance->getConnectionName()) {
                $instance->setConnection($this->connection);
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Definition()
    {
        return $this->hasOne('App\CheckDefinition', 'id', 'check_def_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Crew()
    {
        return $this->belongsTo('App\Crew', 'user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Approvals()
    {
        return $this->hasMany('App\Approval');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     *
     * Check can have many Scans of certificates, licences etc.
     */
    public function Scans()
    {
        return $this->morphMany('App\Scan', 'scanable');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeUnapproved($query)
    {
        return $query->where('renewal_authorised', 0);
    }

    /**
     * @return bool
     */
    public function isValid(Carbon $date = null)
    {
        if($this->Definition()->first()->recurrent == 0 ) return true;  // Lifetime validity

        if(!$date) $date = Carbon::now();

        return ($this->expiry_date >= $date->toDateString());
    }

    /**
     * @return mixed
     */
    public function scanRequired()
    {
        return $this->Definition()->first()->scan_required;
    }

    /**
     * @return mixed
     */
    public function isRecurrent()
    {
        return $this->Definition()->first()->recurrent;
    }

    /**
     * @return mixed
     */
    private function previousCheck()
    {
        // Find checks for this user, of the same definition
        // Order by date and take the second one
        return static::where('check_def_id', $this->check_def_id)->where('user_id', $this->user_id)->orderBy('expiry_date', 'DESC')->skip(1)->first();
    }

    /**
     * @param $defid
     * @param null $user
     * @return bool
     */
    public static function isAuthorised($defid, $user = null)
    {
        if(!$user) $user = Auth::user();

        $check = static::getLatestCheck($user->id, $defid);

        if($check)
        {
            return $check->RenewalAuthorised();
        }

        return false;
    }

    /**
     * @return string
     */
    public function Approver()
    {
        $return = '';

        $approvals = $this->Approvals()->get();

        foreach($approvals as $approval)
        {
            if($approval->User()->first()) {
                $return .= $approval->User()->first()->firstname . ' ' . $approval->User()->first()->lastname  . ' & ';
            } else {
                $return = 'User Deleted';
            }
        }

        return rtrim($return, " & ");
    }

    /**
     * @return mixed
     */
    public function NumberOfApprovals()
    {
        return $this->Approvals()->count();
    }


    /**
     *
     *
     *
     * @return bool
     */
    public function UserHasApproved()
    {
        $approvals = $this->Approvals()->get();

        foreach($approvals as $approval)
        {
            if($approval->User()->first()) {
                if($approval->User()->first()->id == Auth::user()->id) return true;
            }
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public function getStatus(Carbon $date)
    {
        $this->status = $this->calculateStatus($date);
        $this->expires_in = daysToExpiry($this->expiry_date, $date);
        $this->save();

        return $this->status;
    }

    /**
     * @return bool|string
     */
    public function calculateStatus($date)
    {
        // Find the definition properties of this check
        $definition = $this->Definition()->first();

        if(($definition->recurrent == 0) && $this->RenewalAuthorised()) return VALID_FOREVER;

        // If check is in date, Approved by Ops AND VALID, no problem
        if ($this->RenewalAuthorised()) {
            if ($this->isValid($date)) return VALID;
            if ($definition->generate_restriction_on_expiry) {
                return VALID_WITH_RESTRICTION;
            }
            return INVALID;
        }

        // If in date, but not Approved by Ops
        if($this->isValid($date))
        {
            if($definition->generate_restriction_on_expiry) {
                return VALID_WITH_RESTRICTION;
            }

            return VALID_PENDING_APPROVAL;
        }

        // Not in date, Not authorised, but generates restriction, so can be restricted status
        if($definition->generate_restriction_on_expiry) {
            return VALID_WITH_RESTRICTION;
        }

        // Not approved, not valid, no restriction applies
        return INVALID;
    }

    /**
     * @return mixed
     */
    public static function numberToAuthorise()
    {
        return static::where('operator_id', session('operator.id'))->where('renewal_authorised',0)->count();
    }

    /**
     * @return bool
     */
    public function RenewalAuthorised()
    {
        if (count($this->Approvals()->get()) == APPROVALS_REQUIRED) return true;

        return false;
    }

    public function FormattedStatus()
    {
        // FIX LIFETIME BUG
        if($this->status == VALID_FOREVER) return "<strong style='color: blue;'>Lifetime</strong>";

        $date = Carbon::parse($this->expiry_date);

        $formatter = new CheckOverviewExpiryFormatter($date, $this->expires_in);

        return $formatter->output();
    }

    public function expiryDateDMY()
    {
        $date = Carbon::parse($this->expiry_date);

        return $date->format('d/m/Y');
    }

    public function expiryDateWithLifetime()
    {
        if($this->status == VALID_FOREVER) return "<strong style='color: blue;'>Lifetime</strong>";

        $date = Carbon::parse($this->expiry_date);

        return $date->format('d/m/Y');
    }


    public function createdAtFormatted()
    {
        $date = Carbon::parse($this->created_at);

        return $date->format('d/M/Y');
    }

    public function datePerformedFormatted()
    {
        $date = Carbon::parse($this->date_performed);

        return $date->format('d/M/Y');
    }

    public function datePerformedDMY()
    {
        $date = Carbon::parse($this->date_performed);

        return $date->format('d/m/Y');
    }

    public function datePerformedFormattedUS()
    {
        $date = Carbon::parse($this->date_performed);

        return $date->format('Y-m-d');
    }

}

