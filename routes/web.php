<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'DashboardController@index');
Route::get('/home/{id}', 'DashboardController@showByID');
Route::post('/home/', 'DashboardController@show');

// Check Definitions
Route::get('/checks/definitions', 'CheckDefinitionController@index');
Route::get('/checks/definitions/create', 'CheckDefinitionController@create');
Route::get('/checks/definitions/{id}/edit', 'CheckDefinitionController@edit');
Route::post('/checks/definitions/store', 'CheckDefinitionController@store');
Route::post('/checks/definitions/{id}/update', 'CheckDefinitionController@update');
Route::delete('/checks/definitions/delete', 'CheckDefinitionController@destroy');

// Check Groups
Route::get('/checks/groups', 'CheckGroupController@index');
Route::get('/checks/groups/create', 'CheckGroupController@create');
Route::post('/checks/groups/create', 'CheckGroupController@store');
Route::get('/checks/groups/{id}/edit', 'CheckGroupController@edit');
Route::get('/checks/groups/{id}/delete', 'CheckGroupController@destroy');

Route::post('/checks/groups/add', 'CheckGroupController@addCheckDefinitionToGroup');
Route::get('/checks/groups/delete/check/{group_id}/{check_id}', 'CheckGroupController@deleteCheckDefinitionFromGroup');
Route::get('/checks/groups/{group_id}/remove/{extension_id}', 'CheckGroupController@detachExtensionFromGroup');
Route::get('/checks/{checkid}/revalidate/{userid}', 'CheckController@revalidate');
Route::post('/checks/{id}/revalidate', 'CheckController@revalidateStore');
Route::get('/checks/{checkid}/approve/{crewid}', 'CheckController@approve');
Route::get('/checks/{id}/edit/{crewid}', 'CheckController@edit');
Route::get('/checks/{id}/delete/{crewid}', 'CheckController@delete');
Route::post('/checks/{id}/update/{crewid}', 'CheckController@update');

// Archive
Route::get('/checks/{definition_id}/archive/{userid}', 'CheckController@archive');

// Overview
Route::get('/checks/overview/{id}', 'CheckOverviewController@index');

// Scans
Route::post('/checks/uploader/', 'ScanController@upload');

// Crew Groups
Route::get('/crew/groups', 'GroupController@index');
Route::get('/crew/groups/create', 'GroupController@create');
Route::get('/crew/groups/{id}/edit', 'GroupController@edit');
Route::delete('/crew/groups/{id}', 'GroupController@destroy');
Route::post('/crew/groups', 'GroupController@store');
Route::get('/crew/groups/{id1}/applyTo/{id2}', 'CheckGroupController@applyCheckGroupToCrewGroup');
Route::get('/crew/groups/{id1}/removeFrom/{id2}', 'CheckGroupController@removeCheckGroupFromCrewGroup');
Route::post('/crew/groups/add/{id}', 'GroupController@add');
Route::get('/crew/groups/remove/{groupid}/{crewid}', 'GroupController@remove');
